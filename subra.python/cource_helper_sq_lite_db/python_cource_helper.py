import sqlite3 as lite

# Functionality goes here

db_con = None


class DataBaseManage(object):

    def __init__(self):
        super().__init__()
        global db_con
        try:
            db_con = lite.connect('courses.db')
            with db_con:
                cur = db_con.cursor()
                cur.execute(
                    "CREATE TABLE IF NOT EXISTS course(Id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, desc TEXT, price TEXT, is_private BOOLEAN NOT NULL DEFAULT 1)")

        except Exception as e:
            print("Unable to create a db!! ", e)

    def insert_data(self, data):
        try:
            with db_con:
                cur = db_con.cursor()
                cur.execute(
                    "INSERT INTO course(name, desc, price, is_private) VALUES (?, ?, ?, ?)", data
                )
                return True

        except Exception as e:
            return False
        pass

    def delete_data(self, id):
        try:
            with db_con:
                cur = db_con.cursor()
                sql_delete = "DELETE FROM course WHERE id = ?"
                cur.execute(sql_delete, [id])
                return True

        except Exception as e:
            return False

    def fetch_data(self):
        try:
            with db_con:
                cur = db_con.cursor()
                cur.execute(
                    "SELECT * FROM course"
                )
                return cur.fetchall()

        except Exception as e:
            return False

# TODO: Provide interface for user


def main():
    print("*" * 40)
    print("\n:: CORCSE MANAGEMENT :: \n")
    print("*" * 40)
    print("\n")

    db = DataBaseManage()
    print("#" * 40)
    print("\n:: User Manual :: \n")
    print("#" * 40)
    print("\n")
    print("1. Insert New Course")
    print("2. View all Courses")
    print("3. Delete a course - needs id")
    print("#" * 40)
    print("\n")
    choice = input("Enter your choise: ")
    if choice == "1":
        name = input("\nEnter course: ")
        desc = input("\nEnter desc: ")
        price = input("\nEnter price: ")
        private = input("\nIs this private (0/1): ")
        if db.insert_data([name, desc, price, private]):
            print("Course inserted succesfully\n")
        else:
            print("ERRRRROR\n")
    elif choice == "2":
        print("\n:: course list :: \n")
        for index, course in enumerate(db.fetch_data()):
            print("\n Sl No : " + str(index + 1))
            print("Course Id : " + str(course[0]))
            print("Course Name : " + str(course[1]))
            print("Course Desc : " + str(course[2]))
            print("Course price : " + str(course[3]))
            print("Is course Private? : " + 'Yes' if course[4] else 'No')
            print("\n")
    elif choice == "3":
        id = input("\nEnter Id to delete: ")
        if db.delete_data(id):
            print("Course deleted succesfully\n")
        else:
            print("ERRRRROR\n")
    else:
        print("\n BAD choice")


if __name__ == "__main__":
    main()
