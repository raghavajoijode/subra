import my_module as my


def print_hello_world():
    print("Hello World")


if __name__ == "__main__":
    print_hello_world();
    try:
        for a in my.my_range(0.5, 2, 0.2):
            print(f"{a:.2f}")
    except Exception as e:
        print(e)
