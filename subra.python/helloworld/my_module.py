def my_range(*args):
    __check_attributes__(args)
    args_length = len(args)
    start, end, step = 0, 0, 1
    if args_length == 1:
        end, = args
    elif args_length == 2:
        start, end = args
    elif args_length == 3:
        start, end, step = args
    else:
        raise AttributeError("Enter 'at least one or maximum of three' argument")
    return __range__(end, start, step)


def get_type(el):
    print(f"Type of {el} is {type(el)}")


def __range__(end, start, step):
    while start < end:
        yield start
        start += step


def __check_attributes__(args):
    for i in args:
        if not (isinstance(i, int) or isinstance(i, float)):
            raise TypeError("Attribure types must be either 'int' or 'float'")
