from Utils.helpers import Deck, Hand, Chips
import os


class BackJack:
    playing = True

    def __clear(self): return os.system('cls')

    def __show_some(self, player_hand, dealer_hand):
        self.__clear()
        print('Dealer Cards:')
        print('<HIDDEN CARD>')
        print(dealer_hand.cards[1])
        print('Player Cards:')
        print(*player_hand.cards, sep='\n')

    def __show_all(self, player_hand, dealer_hand):
        self.__clear()
        print('Dealer Cards:')
        print(*dealer_hand.cards, sep='\n')
        print('Player Cards:')
        print(*player_hand.cards, sep='\n')

    def __take_bet(self, chips):
        while True:
            try:
                bet_amount = int(input('Enter Chips you wish to bet : '))
            except:
                print('Please enter integer only')
            else:
                if bet_amount > chips.total:
                    print("Sorry, your bet can't exceed", chips.total)
                else:
                    chips.bet = bet_amount
                    break

    def __hit(self, deck, hand):
        hand.add_card(deck.deal())

    def __hit_or_stand(self, deck, hand):
        while True:
            choice = input("Do you like to hit or stand, use 'h' or 's' : ")
            if choice[0].lower() == 'h':
                self.__hit(deck, hand)  # hit() function defined above
            elif choice[0].lower() == 's':
                print("Player stands. Dealer is playing.")
                BackJack.playing = False
            else:
                print("Sorry, please try again.")
                continue
            break

    def __player_busts(self, player, dealer, chips):
        print("Player busts!")
        chips.lose_bet()

    def __player_wins(self, player, dealer, chips):
        print("Player wins!")
        chips.win_bet()

    def __dealer_busts(self, player, dealer, chips):
        print("Dealer busts!")
        chips.win_bet()

    def __dealer_wins(self, player, dealer, chips):
        print("Dealer wins!")
        chips.lose_bet()

    def __push(self, player, dealer):
        print("Dealer and Player tie! It's a push.")

    def game(self):
        player_chips = Chips()
        while True:

            # creating deck and shuffling it
            deck = Deck()
            deck.shuffle()

            player_hand = Hand()
            player_hand.add_card(deck.deal())
            player_hand.add_card(deck.deal())

            dealer_hand = Hand()
            dealer_hand.add_card(deck.deal())
            dealer_hand.add_card(deck.deal())

            self.__take_bet(player_chips)

            self.__show_some(player_hand, dealer_hand)

            while BackJack.playing:
                self.__hit_or_stand(deck, player_hand)
                self.__show_some(player_hand, dealer_hand)
                if player_hand.value > 21:
                    self.__player_busts(player_hand, dealer_hand, player_chips)
                    break

            if player_hand.value <= 21:

                while dealer_hand.value < 17:
                    self.__hit(deck, dealer_hand)

                self.__show_all(player_hand, dealer_hand)

                if dealer_hand.value > 21:
                    self.__dealer_busts(player_hand, dealer_hand, player_chips)
                elif dealer_hand.value > player_hand.value:
                    self.__dealer_wins(player_hand, dealer_hand, player_chips)
                elif dealer_hand.value < player_hand.value:
                    self.__player_wins(player_hand, dealer_hand, player_chips)
                else:
                    self.__push(player_hand, dealer_hand)

            print("\nPlayer's winnings stand at", player_chips.total)

            if player_chips.total == 0:
                print('Not enough chips, restart game')
                break

            new_game = input(
                "Would you like to play another hand? Enter 'y' or 'n' ")
            if new_game[0].lower() == 'y':
                BackJack.playing = True
                continue
            else:
                break
