package org.subra.aem.flagapp.internal.services.v1;

import java.util.List;

import org.subra.aem.flagapp.internal.vo.FlagVo;
import org.subra.aem.flagapp.internal.vo.ProjectVo;

/**
 * The Service Interface to create, update, read and delete flag
 * 
 * @author Raghava Joijode
 *
 */
public interface FlagService {

	<T> void createOrUpdateFlag(final String projectName, final String flagName, final T flagValue);

	List<FlagVo> build(final String name);

	Object readFlag(final String projectName, final String flagName);

	void deleteFlag(final String projectName, final String flagName);

	ProjectVo createProject(final String name);

	ProjectVo readProject(final String name);

	void deleteProject(String name);

	List<ProjectVo> projects();

}
