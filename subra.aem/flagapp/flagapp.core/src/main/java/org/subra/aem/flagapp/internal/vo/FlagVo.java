package org.subra.aem.flagapp.internal.vo;

public class FlagVo extends FMSVo {

	private boolean isBoolean;

	private Object value;

	public boolean isBoolean() {
		return isBoolean;
	}

	public void setBoolean(boolean isBoolean) {
		this.isBoolean = isBoolean;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
