package org.subra.aem.flagapp.internal.components.models.v1;

import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.subra.aem.commons.helpers.RequestParser;
import org.subra.aem.flagapp.internal.services.v1.FlagService;
import org.subra.aem.flagapp.internal.vo.FlagVo;
import org.subra.aem.flagapp.internal.vo.ProjectVo;
import org.subra.aem.flagapp.models.FMSModel;
import org.subra.aem.flagapp.utils.FlagManagementUtil;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, adapters = { FMSModel.class })
public class FMSModelImpl implements FMSModel {

	@OSGiService
	private FlagService fms;

	@Self
	private SlingHttpServletRequest request;

	private String projectName;

	@PostConstruct
	protected void init() {
		projectName = RequestParser.getFirstSelector(request).orElse(null);
	}

	@Override
	public String getProject() {
		return projectName;
	}

	@Override
	public List<ProjectVo> getProjects() {
		List<ProjectVo> projects = fms.projects();
		projects.sort(Comparator.comparing(ProjectVo::getName));
		return projects;
	}

	@Override
	public List<FlagVo> getFlags() {
		return fms.build(projectName);
	}

	@Override
	public Object getMessage() {
		System.out.println(FlagManagementUtil.readFlag("test", "test1"));
		return FlagManagementUtil.readFlag("test", "test1");
	}
}
