package org.subra.aem.flagapp.internal.services.v1.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.exceptions.SubraApiException;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.commons.jcr.utils.SubraResourceUtils;
import org.subra.aem.commons.utils.SubraDateTimeUtil;
import org.subra.aem.flagapp.internal.services.v1.FlagService;
import org.subra.aem.flagapp.internal.vo.FlagVo;
import org.subra.aem.flagapp.internal.vo.ProjectVo;

/**
 * The Service Implementaion to create, update, read and delete flag
 * 
 * @author Raghava Joijode
 *
 */
@Component(service = FlagService.class, immediate = true)
public class FlagServiceImpl implements FlagService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlagServiceImpl.class);

	private static final String PN_FMS_CREATED_ON = "fms-createdOn";
	private static final String PN_FMS_UPDATED_ON = "fms-updatedOn";
	private static final String PN_FMS_IS_BOOLEAN = "fms-isBoolean";
	private static final String PN_FMS_TEMP_CREATENOW = "fms-created-now";
	private static final String PN_FMS_TITLE = "fms-title";
	private static final String PN_FMS_VALUE = "fms-value";

	private static final String CONFIG_NODE = "config";

	private Resource rootNode;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	/**
	 * Method to create a new flag or update if already exists
	 * 
	 * @param flagTitle The flag which needs to be created or updated
	 * @param flagValue The value to be associate with the flag
	 * @param type      The type of the value associated with the flag
	 */
	@Override
	public <T> void createOrUpdateFlag(final String projectName, final String flagTitle, final T flagValue) {
		try {
			Resource project = getProjectResource(projectName);
			ResourceResolver resolver = project.getResourceResolver();
			Optional.of(project).map(p -> getChildResource(p, CONFIG_NODE))
					.map(c -> createOrGetChildResource(resolver, c, flagTitle)).ifPresent(flag -> {
						try {
							ModifiableValueMap mvm = flag.adaptTo(ModifiableValueMap.class);
							if (Boolean.FALSE.equals(mvm.get(PN_FMS_TEMP_CREATENOW, false)))
								mvm.put(PN_FMS_UPDATED_ON, SubraDateTimeUtil.localDateTimeString());
							mvm.put(PN_FMS_VALUE, flagValue);
							mvm.put(PN_FMS_TITLE, flagTitle);
							mvm.put(PN_FMS_IS_BOOLEAN, flagValue instanceof Boolean);
							mvm.remove(PN_FMS_TEMP_CREATENOW);
							resolver.commit();
						} catch (PersistenceException e) {
							throw new SubraApiException("Error Creating Flag...");
						}
					});
		} catch (NullPointerException e) {
			throw new SubraApiException("Project Not Found...");
		}
	}

	/**
	 * Method to read the flag value given flag name
	 * 
	 * @param flagName The name of the flag whose value needs to be returned
	 * @return The value associate with the flag
	 */
	@Override
	public Object readFlag(final String projectName, final String flagName) {
		return Optional.ofNullable(createOrGetRootNode()).map(r -> r.getChild(projectName))
				.map(p -> p.getChild(CONFIG_NODE)).map(c -> c.getChild(flagName)).map(Resource::getValueMap)
				.map(vm -> vm.get(PN_FMS_VALUE)).orElseThrow(
						() -> new SubraApiException("Flag Not Found - Either of Project or Flag Doesn't exists..."));
	}

	/**
	 * Method to delete the given flag
	 * 
	 * @param flagName The flag which needs to be deleted
	 */
	@Override
	public void deleteFlag(final String projectName, final String flagName) {
		try {
			Resource project = getProjectResource(projectName);
			ResourceResolver resolver = project.getResourceResolver();
			Optional.of(getProjectResource(projectName)).map(p -> getChildResource(p, CONFIG_NODE))
					.map(c -> getChildResource(c, flagName)).ifPresent(flag -> {
						try {
							deleteResource(resolver, flag);
						} catch (PersistenceException e) {
							throw new SubraApiException(e);
						}
					});
		} catch (NullPointerException e) {
			throw new SubraApiException(e);
		}
	}

	@Override
	public ProjectVo createProject(final String name) {
		Resource root = createOrGetRootNode();
		ResourceResolver resolver = root.getResourceResolver();
		try {
			Optional.of(root).map(r -> r.getChild(name)).ifPresentOrElse(p -> {
				if (p.getChild(CONFIG_NODE) == null)
					createResource(resolver, p, CONFIG_NODE);
			}, () -> createResource(resolver, createResource(resolver, root, name), CONFIG_NODE));
		} catch (NullPointerException e) {
			LOGGER.error("Error Creating Project...", e);
			throw new SubraApiException("Error Creating Project...");
		}
		return readProject(SubraCommonHelper.createNameFromTitle(name));
	}

	@Override
	public ProjectVo readProject(final String name) {
		ProjectVo projectVo = new ProjectVo();
		Optional.ofNullable(createOrGetRootNode()).map(r -> r.getChild(name)).map(p -> p.getChild(CONFIG_NODE))
				.ifPresentOrElse(c -> setProjectVo(c.getParent(), projectVo), () -> {
					throw new SubraApiException("Project Doesn't Exists...");
				});
		return projectVo;
	}

	@Override
	public void deleteProject(final String name) {
		Optional.ofNullable(createOrGetRootNode()).map(r -> r.getChild(name)).map(p -> p.getChild(CONFIG_NODE))
				.ifPresentOrElse(c -> {
					try {
						deleteResource(c.getResourceResolver(), c.getParent());
					} catch (PersistenceException e) {
						LOGGER.error("Error Deleting Project...", e);
						throw new SubraApiException("Error Deleting Project...");
					}
				}, () -> {
					throw new SubraApiException("Project Doesn't Exists...");
				});
	}

	@Override
	public List<FlagVo> build(final String projectName) {
		List<FlagVo> flags = new ArrayList<>();
		try {
			Optional.ofNullable(createOrGetRootNode()).map(r -> getChildResource(r, projectName))
					.map(p -> getChildResource(p, CONFIG_NODE)).map(Resource::listChildren).ifPresentOrElse(itr -> {
						while (itr.hasNext()) {
							Resource f = itr.next();
							flags.add(createFlagVo(f));
						}
					}, () -> {
						throw new SubraApiException("Flag Not Found - Either of Project or Flag Doesn't exists...");
					});
		} catch (NullPointerException e) {
			throw new SubraApiException("Error Building Project");
		}
		return flags;
	}

	@Override
	public List<ProjectVo> projects() {
		List<ProjectVo> projects = new LinkedList<>();
		Optional.ofNullable(createOrGetRootNode()).map(Resource::listChildren).ifPresent(i -> {
			while (i.hasNext()) {
				projects.add(createProjectVo(i.next()));
			}
		});
		return projects;
	}

	private ProjectVo createProjectVo(Resource project) {
		ProjectVo projectVo = new ProjectVo();
		setProjectVo(project, projectVo);
		return projectVo;
	}

	private void setProjectVo(Resource project, ProjectVo projectVo) {
		projectVo.setTitle(project.getValueMap().get(PN_FMS_TITLE, project.getName()));
		projectVo.setFlagsCount(Optional.ofNullable(project.getChild(CONFIG_NODE)).map(Resource::listChildren)
				.map(IteratorUtils::size).orElse(0));
		projectVo.setName(project.getName());
		projectVo.setCreatedOn(project.getValueMap().get(PN_FMS_CREATED_ON, String.class));
	}

	private FlagVo createFlagVo(Resource flag) {
		FlagVo flagVo = new FlagVo();
		ValueMap vm = flag.getValueMap();
		flagVo.setTitle(vm.get(PN_FMS_TITLE, flag.getName()));
		flagVo.setName(flag.getName());
		flagVo.setBoolean(vm.get(PN_FMS_IS_BOOLEAN, false));
		flagVo.setValue(vm.get(PN_FMS_VALUE));
		flagVo.setCreatedOn(vm.get(PN_FMS_CREATED_ON, String.class));
		flagVo.setUpdatedOn(vm.get(PN_FMS_UPDATED_ON, String.class));
		return flagVo;
	}

	private Resource getProjectResource(final String name) {
		return Optional.ofNullable(createOrGetRootNode()).map(r -> r.getChild(name)).orElseGet(() -> null);
	}

	private Resource getChildResource(final Resource base, final String childTitle) {
		return base.getChild(SubraCommonHelper.createNameFromTitle(childTitle));
	}

	private Resource createOrGetChildResource(final ResourceResolver resolver, final Resource base,
			final String title) {
		Resource child = getChildResource(base, title);
		return child != null ? child : createResource(resolver, base, title);
	}

	private Resource createOrGetRootNode() {
		if (rootNode == null) {
			useResourceResolver(resolver -> {
				Resource fms = resolver.getResource("/var/fms");
				rootNode = fms != null ? fms : createResource(resolver, resolver.getResource("/var"), "fms");
			});
		}
		return rootNode;
	}

	private Resource createResource(ResourceResolver resolver, final Resource base, final String resourceTitle) {
		Resource resource = null;
		try {
			resource = resolver.create(base, SubraCommonHelper.createNameFromTitle(resourceTitle),
					composeResourceProperties(resourceTitle));
			resolver.commit();
		} catch (PersistenceException e) {
			LOGGER.error("Error creating root resource..", e);
		}
		return resource;
	}

	private Map<String, Object> composeResourceProperties(final String title) {
		Map<String, Object> properties = new HashMap<>();
		properties.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
		properties.put(PN_FMS_TITLE, title);
		properties.put(PN_FMS_CREATED_ON, SubraDateTimeUtil.localDateTimeString());
		properties.put(PN_FMS_TEMP_CREATENOW, true);
		return properties;
	}

	private void deleteResource(ResourceResolver resolver, Resource flag) throws PersistenceException {
		resolver.delete(flag);
		resolver.commit();
	}

	private void useResourceResolver(Consumer<ResourceResolver> consumer) {
		try {
			ResourceResolver resolver = getServiceResourceResolver();
			consumer.accept(resolver);
		} catch (LoginException e) {
			LOGGER.error("Error Getting ResourceResolver...", e);
		}
	}

	private ResourceResolver getServiceResourceResolver() throws LoginException {
		return SubraResourceUtils.getAdminServiceResourceResolver(resourceResolverFactory);
	}

	@Deactivate
	protected void deactivate() {
		try {
			if (getServiceResourceResolver() != null && getServiceResourceResolver().isLive())
				getServiceResourceResolver().close();
		} catch (LoginException e) {
			LOGGER.error("Error Closing ResourceResolver...", e);
		}
	}

}
