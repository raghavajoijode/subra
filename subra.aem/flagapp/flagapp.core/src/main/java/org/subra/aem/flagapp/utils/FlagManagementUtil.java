package org.subra.aem.flagapp.utils;

import org.osgi.service.cm.ConfigurationException;
import org.subra.aem.flagapp.internal.services.v1.FlagService;

public class FlagManagementUtil {

	private static FlagService flagService;

	private FlagManagementUtil() {
		throw new IllegalStateException(this.getClass().getSimpleName());
	}

	public static void configure(FlagService fms) throws ConfigurationException {
		flagService = fms;
		if (flagService == null) {
			throw new ConfigurationException(null,
					"Either 'author' or 'publish' run modes may be specified, not both.");
		}
	}

	public static Object readFlag(final String projectName, String flagName) {
		return flagService.readFlag(projectName, flagName);
	}

}
