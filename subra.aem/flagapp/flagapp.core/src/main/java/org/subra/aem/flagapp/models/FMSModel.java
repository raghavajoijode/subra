package org.subra.aem.flagapp.models;

import java.util.List;

import org.subra.aem.flagapp.internal.vo.FlagVo;
import org.subra.aem.flagapp.internal.vo.ProjectVo;

public interface FMSModel {

	default String getProject() {
		throw new UnsupportedOperationException();
	}

	default List<ProjectVo> getProjects() {
		throw new UnsupportedOperationException();
	}

	default List<FlagVo> getFlags() {
		throw new UnsupportedOperationException();
	}

	default Object getMessage() {
		throw new UnsupportedOperationException();
	}

}
