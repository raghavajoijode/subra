package org.subra.aem.flagapp.internal.servlets.v2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.subra.aem.commons.constants.SubraHttpType;
import org.subra.aem.commons.exceptions.SubraApiException;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.commons.utils.SubraStringUtils;
import org.subra.aem.flagapp.internal.services.v1.FlagService;

@Component(service = Servlet.class)
@SlingServletResourceTypes(resourceTypes = "sling:nonexisting", methods = HttpConstants.METHOD_GET)
@ServiceDescription("FMS Opting Servlet V2")
public class FlagManagementServlet extends SlingSafeMethodsServlet implements OptingServlet {

	private static final String FLAG = "flag";
	private static final String TARGET = "target";
	private static final String OPERATION = "operation";
	private static final String PROJECT_NAME_GROUP = "pname";
	private static final String FLAG_NAME_GROUP = "fname";
	private static final String PARAM_FTYPE = "ftype";
	private static final String PARAM_FVALUE = "fvalue";
	private static final String PATTERN_ERROR = "PATTERN_ERROR";
	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";
	private static final String RS_FAILURE_REASON = "REASON";
	private static final String RS_STATUS = "STATUS";
	private static final String RS_DATA = "data";
	private static final long serialVersionUID = 1L;
	private static final String PATTERN = "/api/fms/v2/(?<target>project|flag)/(?<operation>create|read|update|delete)(?:/(?<pname>[a-zA-Z0-9-_%]+))?(?:/(?<fname>[a-zA-Z0-9-_%]+))?";

	@Reference
	private FlagService fms;

	@Override
	public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(SubraHttpType.MEDIA_TYPE_JSON.value());
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(request.getRequestURI());
		Map<String, Object> result = new HashMap<>();
		if (matcher.matches()) {
			result = processOperation(request, matcher);
		} else {
			result.put(RS_STATUS, PATTERN_ERROR);
		}
		response.getWriter().write(SubraStringUtils.writeValueAsString(result));
	}

	private Map<String, Object> processOperation(final SlingHttpServletRequest request, final Matcher matcher) {
		Map<String, Object> result = new HashMap<>();
		result.put(RS_STATUS, SUCCESS);
		final String target = matcher.group(TARGET);
		final String operation = matcher.group(OPERATION);
		String projectName = matcher.group(PROJECT_NAME_GROUP);
		String flagName = matcher.group(FLAG_NAME_GROUP);
		if (SubraStringUtils.isNoneBlank(target, operation, projectName)) {
			projectName = StringUtils.replace(projectName, "%20", SubraCommonHelper.SPACE);
			flagName = StringUtils.replace(flagName, "%20", SubraCommonHelper.SPACE);
			switch (operation) {
			case "create":
				processCreate(request, result, target, projectName, flagName);
				break;
			case "read":
				processRead(result, target, projectName, flagName);
				break;
			case "update":
				processUpdate(request, result, target, projectName, flagName);
				break;
			case "delete":
				processDelete(result, target, projectName, flagName);
				break;
			default:
				throw new UnsupportedOperationException(operation + " is not supported...");
			}
		} else if (StringUtils.equalsIgnoreCase(operation, "read") && !StringUtils.equalsIgnoreCase(target, FLAG)) {
			result.put(RS_DATA, fms.projects());
		} else {
			setFailure(result, "Unsupported Operation...");
		}
		return result;
	}

	private void processCreate(final SlingHttpServletRequest request, Map<String, Object> result, final String target,
			final String projectName, final String flagName) {
		try {
			final String fType = request.getParameter(PARAM_FTYPE);
			final String fValue = request.getParameter(PARAM_FVALUE);
			if (StringUtils.equalsIgnoreCase(target, FLAG)) {
				if (StringUtils.isNotBlank(fValue))
					fms.createOrUpdateFlag(projectName, flagName, getFlagValueByType(fValue, fType));
				else
					throw new SubraApiException("Missing Parameters...");
			} else {
				result.put(RS_DATA, fms.createProject(projectName));
			}
		} catch (SubraApiException e) {
			setFailure(result, e.getLocalizedMessage());
		}
	}

	private void setFailure(Map<String, Object> result, String message) {
		result.put(RS_STATUS, FAILURE);
		result.put(RS_FAILURE_REASON, message);
	}

	private void processRead(final Map<String, Object> result, final String target, final String projectName,
			final String flagName) {
		try {
			if (StringUtils.equalsIgnoreCase(target, FLAG)) {
				result.put(RS_DATA,
						StringUtils.isBlank(flagName) ? fms.build(projectName) : fms.readFlag(projectName, flagName));

			} else {
				result.put(RS_DATA, fms.readProject(projectName));
			}
		} catch (SubraApiException e) {
			setFailure(result, e.getLocalizedMessage());
		}
	}

	private void processUpdate(final SlingHttpServletRequest request, Map<String, Object> result, final String target,
			final String projectName, final String flagName) {
		if (StringUtils.equalsIgnoreCase(target, FLAG)) {
			try {
				fms.createOrUpdateFlag(projectName, flagName,
						StringUtils.defaultString(request.getParameter(PARAM_FVALUE)));
			} catch (SubraApiException e) {
				setFailure(result, e.getLocalizedMessage());
			}
		}
	}

	private void processDelete(final Map<String, Object> result, final String target, final String projectName,
			final String flagName) {
		try {
			if (StringUtils.equalsIgnoreCase(target, FLAG)) {
				Object value = fms.readFlag(projectName, flagName);
				if (value != null)
					fms.deleteFlag(projectName, flagName);

				result.put(RS_DATA, value);
			} else {
				fms.deleteProject(projectName);
			}
		} catch (SubraApiException e) {
			setFailure(result, e.getLocalizedMessage());
		}
	}

	private Object getFlagValueByType(String value, String type) {
		if (StringUtils.equalsIgnoreCase(type, "Boolean"))
			return BooleanUtils.toBoolean(value);

		else
			return value;
	}

	@Override
	public boolean accepts(SlingHttpServletRequest request) {
		return Pattern.matches(PATTERN, request.getRequestURI());
	}
}
