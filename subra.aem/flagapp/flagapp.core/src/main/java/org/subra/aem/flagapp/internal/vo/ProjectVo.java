package org.subra.aem.flagapp.internal.vo;

public class ProjectVo extends FMSVo {

	private long flagsCount;

	public long getFlagsCount() {
		return flagsCount;
	}

	public void setFlagsCount(long flagsCount) {
		this.flagsCount = flagsCount;
	}

}
