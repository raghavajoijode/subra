package org.subra.aem.foundation.example.workflow.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssetFile implements Comparable<AssetFile> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssetFile.class);

	private String name;
	private String imageType;
	private String modifiedBy;
	private String modifiedDate;
	private String path;
	private String title;
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int compareTo(AssetFile other) {

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

		Date file1Date = null;
		Date file2Date = null;
		try {

			file1Date = formatter.parse(modifiedDate);
			file2Date = formatter.parse(other.getModifiedDate());
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		}
		if (file1Date == null) {
			return 0;
		} else if (file1Date.after(file2Date)) {
			return 1;
		} else if (file1Date.before(file2Date)) {
			return -1;
		} else {
			return 0;
		}

	}

	@Override
	public boolean equals(Object object) {
		return object instanceof AssetFile && ((AssetFile) object).getPath() == this.path;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
