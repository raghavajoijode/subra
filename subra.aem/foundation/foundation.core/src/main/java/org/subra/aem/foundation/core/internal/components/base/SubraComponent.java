package org.subra.aem.foundation.core.internal.components.base;

import com.adobe.cq.export.json.ComponentExporter;

public interface SubraComponent extends ComponentExporter {

	boolean isEmpty();

	@Override
	default String getExportedType() {
		throw new UnsupportedOperationException();
	}

}
