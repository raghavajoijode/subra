package org.subra.aem.foundation.core.helpers;

import java.util.Map;

import org.subra.aem.commons.constants.SubraHttpType;

public interface SubraGenericService {

	void sendExceptionMessage(final String className, final String message);

	void sendElectronicMessage(final String subject, final String message);

	<T> T callBackendService(final String endpointUrl, final String resource, final Map<String, String> requestHeaders,
			final Map<String, String> queryParams, final Object postObject, final SubraHttpType requestType);

}
