package org.subra.aem.foundation.restclient.dtos;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.subra.aem.commons.constants.SubraHttpType;

/**
 * Wrapper for a rest response to provide access to header data
 * 
 * @author raghava
 *
 * @param <T>
 */
public class SubraRestClientResponseDto<T> {

	private T object;
	private Header[] headers;

	/**
	 * @param object
	 * @param response
	 */
	public SubraRestClientResponseDto(final T object, final Header[] headers) {
		this.object = object;
		this.headers = headers;
	}

	/**
	 * Return the object from the response.
	 * 
	 * @return
	 */
	public T getObject() {
		return object;
	}

	/**
	 * Return a header value for the given header name.
	 * 
	 * @param headerName
	 * @return
	 */
	public String getHeaderValue(final SubraHttpType headerName) {
		if (headerName != null && headers != null) {
			return Arrays.stream(headers).filter(h -> StringUtils.equals(h.getName(), headerName.value())).findFirst()
					.map(Header::getValue).orElse(null);
		}
		return StringUtils.EMPTY;
	}
}
