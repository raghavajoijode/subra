package org.subra.aem.foundation.core.dtos.product;

import java.util.Map;

public class ProductsDto {

	private Map<String, ProductDto> products;

	public Map<String, ProductDto> getProducts() {
		return products;
	}

	public void setProducts(Map<String, ProductDto> products) {
		this.products = products;
	}

}