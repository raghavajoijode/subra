package org.subra.aem.foundation.core.services.impl;

import java.util.List;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraHttpType;
import org.subra.aem.ehcache.services.CacheService;
import org.subra.aem.foundation.core.helpers.impl.SubraGenericServiceImpl;
import org.subra.aem.foundation.core.services.TicketService;
import org.subra.common.api.dtos.TicketDto;

@Component(immediate = true, service = TicketService.class)
@Designate(ocd = TicketServiceImpl.Config.class)
public class TicketServiceImpl extends SubraGenericServiceImpl implements TicketService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TicketServiceImpl.class);

	private static final String TICKET_API_URL_TEMPLATE = "tickets.api.url.template";

	private String ticketsApiUrlTemplate;

	private static final String TICKET_API_URL_VALUE = "http://localhost:8080/subra/api/v1/tickets";

	@ObjectClassDefinition(name = "Subra - TicketServiceImpl configuration", description = "TicketServiceImpl configuration")
	public @interface Config {
		@AttributeDefinition(name = TICKET_API_URL_TEMPLATE, description = "Template url for ticket service")
		String ticketApiUrlTemplate() default TICKET_API_URL_VALUE;
	}

	@Activate
	@Modified
	private void init(final Config config) {
		LOGGER.trace("Activate TicketsServiceImpl");
		ticketsApiUrlTemplate = config.ticketApiUrlTemplate();
	}

	@Reference
	CacheService cacheManager;

	@Override
	public List<TicketDto> getTickets() {
		return callBackendService(ticketsApiUrlTemplate, null, null, null, null, SubraHttpType.GET);
	}

	@Override
	public TicketDto getTicket(String number) {
		return callBackendService(ticketsApiUrlTemplate, number, null, null, null, SubraHttpType.GET);
	}

}
