package org.subra.aem.foundation.core.internal.components.base;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;
import org.subra.aem.commons.jcr.utils.SubraResourceUtils;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.SlingModelFilter;
import com.day.cq.wcm.api.designer.Style;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Base abstract class for Sling Models which implements common methods involved
 * for each Sling Model like getting child items, their order and style system
 * 
 * @author Raghava Joijode
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class }, adapters = { AbstractComponentModel.class,
		ComponentExporter.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public abstract class AbstractComponentModel {

	@Self
	private SlingHttpServletRequest request;

	@OSGiService
	private ModelFactory modelFactory;

	@OSGiService
	private SlingModelFilter slingModelFilter;

	@ScriptVariable
	private Style currentStyle;

	@SlingObject
	private ResourceResolver resolver;

	@ValueMapValue(name = "cq:styleIds")
	@Optional
	private List<String> styleids;

	protected boolean isEmpty;

	private Map<String, ComponentExporter> childModels;

	/**
	 * To get the map of child resource and the models associated with each child
	 * resource
	 * 
	 * @return Map of child nodes paired with resource name and model associated
	 *         with resource
	 */
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty(value = ":items")
	public Map<String, ? extends ComponentExporter> getExportedItems() {
		if (childModels == null) {
			childModels = SubraResourceUtils.getChildModels(request, slingModelFilter, modelFactory,
					ComponentExporter.class);
		}
		return childModels;
	}

	/**
	 * TO get the list of child node names in the order they are placed in the JCR
	 * 
	 * @return Array of child node names
	 */
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty(value = ":itemsOrder")
	public String[] getExportedItemsOrder() {
		Map<String, ? extends ComponentExporter> models = getExportedItems();
		if (models.isEmpty()) {
			return ArrayUtils.EMPTY_STRING_ARRAY;
		}
		return models.keySet().toArray(ArrayUtils.EMPTY_STRING_ARRAY);
	}

	/**
	 * To return the css classes associated to the component via style system
	 * (mapped with help of styleid)
	 * 
	 * @return List of classes associated with style ids
	 */
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty(value = ":styleSystems")
	public List<String> getStyleSystemClasses() {
		return SubraResourceUtils.getAssociatedStyleSystem(resolver.getResource(currentStyle.getPath()), styleids);
	}

	public boolean isEmpty() {
		return false;
	}

}
