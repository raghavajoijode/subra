/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.subra.aem.foundation.core.filters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.contentsync.PageExporter;

/**
 * Simple servlet filter component that logs incoming requests.
 */
@Component(service = Filter.class, property = { Constants.SERVICE_DESCRIPTION
		+ "=Service servlet filter component that manipulates incoming requests that matches the pattern and checks if it is valid JSON file then sends it as response",
		EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
		Constants.SERVICE_RANKING + "=-700",
		"sling.filter.pattern=" + HtmlExporterFilter.EXPORT_HTML_PREFIX + "(.*)" + HtmlExporterFilter.ZIP_EXTENSION })

@ServiceDescription("Html pageExporter filter filter incoming requests")
@ServiceVendor("Subra")
public class HtmlExporterFilter implements Filter {

	static final String ZIP_EXTENSION = ".zip";

	static final String EXPORT_HTML_PREFIX = "/export/html/";

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Reference
	private PageExporter pageExporter;

	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException {

		final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) servletRequest;
		final HttpServletResponse response = (HttpServletResponse) servletResponse;

		try (ResourceResolver resourceResolver = slingRequest.getResourceResolver()) {
			final String reqType = slingRequest.getParameter("requestType");
			// Getting Page path and page...
			final String requestPagePath = slingRequest.getRequestPathInfo().getResourcePath()
					.replace(EXPORT_HTML_PREFIX, "/").replace(ZIP_EXTENSION, "").trim();
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page page = pageManager.getPage(requestPagePath);
			if (page == null) {
				response.getWriter().write("Page DOsent exist");
				throw new Exception("Page Not Found...");
			}
			response.setContentType("application/zip");
			response.addHeader("Content-Disposition", "attachment;filename=\"" + page.getName()
					+ String.valueOf(System.currentTimeMillis()) + ZIP_EXTENSION + "\"");

			if (reqType != null && reqType.equalsIgnoreCase("dam"))
				exportPageViaDamFile(page, resourceResolver, response);
			else
				exportPageFromResponse(page, resourceResolver, response);

			response.flushBuffer();
		} catch (WCMException we) {
			log.error("WCMException occured...." + we);
		} catch (RepositoryException | IOException e) {
			log.error("RepositoryException or RepositoryException occured...." + e);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void exportPageFromResponse(Page page, ResourceResolver resourceResolver, HttpServletResponse response)
			throws WCMException {
		pageExporter.export(page, resourceResolver, response);
	}

	private void exportPageViaDamFile(Page page, ResourceResolver resourceResolver, HttpServletResponse response)
			throws WCMException, PathNotFoundException, RepositoryException, IOException {
		final String zipFileName = page.getName() + String.valueOf(System.currentTimeMillis()) + ZIP_EXTENSION;
		final String tmpDamFilePath = "/content/dam/foundation/" + zipFileName;
		pageExporter.export(page, resourceResolver, tmpDamFilePath);
		Node contentNode = resourceResolver.getResource(tmpDamFilePath + "/jcr:content").adaptTo(Node.class);
		Property data = contentNode.getProperty("jcr:data");
		OutputStream os = response.getOutputStream();
		try (InputStream is = data.getBinary().getStream()) {
			IOUtils.copy(is, os);
		} finally {
			resourceResolver.delete(resourceResolver.getResource(tmpDamFilePath));
			resourceResolver.commit();
		}

	}

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void destroy() {
	}

}