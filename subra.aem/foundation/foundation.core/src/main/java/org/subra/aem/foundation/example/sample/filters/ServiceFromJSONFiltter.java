package org.subra.aem.foundation.example.sample.filters;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.engine.EngineConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

@Component(service = Filter.class, property = { Constants.SERVICE_DESCRIPTION
		+ "=Service servlet filter component that manipulates incoming requests that matches the pattern and checks if it is valid JSON file then sends it as response",
		EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
		Constants.SERVICE_RANKING + "=-700", "sling.filter.pattern=/services/api/.*" })
@Designate(ocd = ServiceFromJSONFiltter.Config.class)
public class ServiceFromJSONFiltter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceFromJSONFiltter.class);

	private String rootDAMPath;

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
			throws IOException, ServletException {
		final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
		final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
		slingResponse.setContentType("application/json");
		String extension = slingRequest.getRequestPathInfo().getExtension();
		String regex = extension != null ? "." + extension : "";
		String assetFinalPath = rootDAMPath + slingRequest.getRequestPathInfo().getResourcePath().replace(regex, "");
		assetFinalPath = assetFinalPath.trim();
		if (extension != null && extension.contains("xml")) {
			slingResponse.setContentType("application/xml");
			slingResponse.getWriter().print(getXMLFromJSON(assetFinalPath, slingRequest));
		} else {
			slingResponse.getWriter().print(getJsonFromJSONAsset(assetFinalPath, slingRequest));
		}
	}

	@Override
	public void init(FilterConfig filterConfig) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void destroy() {
		throw new UnsupportedOperationException();
	}

	@Activate
	@Modified
	protected void activate(Config config) {
		rootDAMPath = config.servicesBasePath();
	}

	@ObjectClassDefinition(name = "Service Filter Property Configurations")
	public @interface Config {
		@AttributeDefinition(name = "Services Dam Path", description = "Path where service jsons are stored; ex: = '/content/dam/demo-services'", type = AttributeType.STRING)
		String servicesBasePath() default "/content/dam";
	}

	private JsonObject getJsonFromJSONAsset(String assetFinalPath, SlingHttpServletRequest slingRequest) {
		JsonObject returnObject = new JsonObject();
		Resource fileResource = slingRequest.getResourceResolver().getResource(assetFinalPath + ".json"); // Converting
		try {
			// validating if valid asset exists with above path and its of type JSON.
			if (fileResource != null && ("application/json").equals(fileResource.adaptTo(Asset.class).getMimeType())) {
				Asset asset = fileResource.adaptTo(Asset.class);
				returnObject = getJsonFromAsset(asset);
			} else {
				returnObject.addProperty("ERROR", "JSON_FILE_MISSING_" + assetFinalPath);
			}
		} catch (JsonParseException e) {
			LOGGER.error("JSONException_OCCURED_In {} . The Exception is - {}", this.getClass().getName(),
					e.getMessage());
		}
		return returnObject;
	}

	private JsonObject getJsonFromAsset(Asset asset) {
		JsonObject returnObject = new JsonObject();
		try (InputStreamReader is = new InputStreamReader(asset.getOriginal().getStream(), StandardCharsets.UTF_8)) {
			returnObject = (JsonObject) new JsonParser().parse(is);
		} catch (IOException e) {
			LOGGER.error("IOException_OCCURED_In {} . The Exception is - {}", this.getClass().getName(),
					e.getMessage());
		}
		return returnObject;
	}

	private String getXMLFromJSON(String assetFinalPath, SlingHttpServletRequest slingRequest) {
		JsonObject returnObject = getJsonFromJSONAsset(assetFinalPath, slingRequest);
		String xml = null;
		try {
			JSONObject jsonObject = new JSONObject(returnObject.toString());
			xml = XML.toString(jsonObject);
		} catch (JSONException e) {
			LOGGER.error("JSONException_OCCURED_In {} . The Exception is - {}", this.getClass().getName(),
					e.getMessage());
		}
		return xml;
	}

}