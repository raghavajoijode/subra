package org.subra.aem.foundation.core.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BreadcrumbItem {

	private String label;
	private String link;
	private boolean active;

	@Override
	public String toString() {
		return "[Breadcrumb : " + super.toString() + "]";
	}
}