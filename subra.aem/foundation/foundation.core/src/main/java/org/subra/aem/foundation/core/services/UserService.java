package org.subra.aem.foundation.core.services;

import org.subra.common.api.dtos.account.User;

public interface UserService {
	User getUser(String number);
}
