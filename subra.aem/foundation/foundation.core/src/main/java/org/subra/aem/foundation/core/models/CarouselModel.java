package org.subra.aem.foundation.core.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ResourcePath;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.subra.aem.commons.jcr.utils.SubraResourceUtils;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.DamUtil;

@Model(adaptables = SlingHttpServletRequest.class)
public class CarouselModel {
	
	private static final String RANDOM_IMAGE_CAROUSEL_TYPE = "random-child-images";
	private static final String FIXED_LIST_CAROUSEL_TYPE = "fixed-list";
	
    @ScriptVariable
    private ValueMap properties;

    @SlingObject
    private Resource resource;
    
    @Self
    private SlingHttpServletRequest request;

    @ChildResource(name="carouselItems", via = "resource")
    private Resource carouselItemsResource;
    
    @ResourcePath(name = "parentPath")
    private Resource randomImagesParentResource;
    
    private List<CarouselItem> items;
    
    @ValueMapValue(via = "resource") 
    @Optional
    private String carouselType;
    
    @ValueMapValue(via = "resource") 
    @Default(intValues = 8) 
    private int numberOfItems;
    
    @ValueMapValue(via = "resource") 
    @Optional
    private int bucketCount;
    
    

    @PostConstruct
    private void initModel() {
    	bucketCount = bucketCount > numberOfItems * 10 ? bucketCount : numberOfItems * 200; 
        if (CollectionUtils.isEmpty(items)) {
            items = FIXED_LIST_CAROUSEL_TYPE.equalsIgnoreCase(carouselType) ? createFixedCarouselItems() : createRandomCarouselItems();
        }
    }

    public Collection<CarouselItem> getCarouselItems() {
        return Collections.unmodifiableList(items);
    }

    private List<CarouselItem> createFixedCarouselItems() {
        return SubraResourceUtils.adoptChildNodestoModel(carouselItemsResource, CarouselItem.class);
    }
    
    private List<CarouselItem> createRandomCarouselItems() {
        return getAssetsFromParent(randomImagesParentResource, new ArrayList<>()).stream().limit(numberOfItems).map(this::createCarouselItemFromImage).collect(Collectors.toList());
    }

	private List<Asset> getAssetsFromParent(Resource resource, List<Asset> assets) {
    	Iterator<Resource> children = resource.listChildren();
    	children.forEachRemaining(child -> {
    		Asset asset = child.adaptTo(Asset.class);
        	if(DamUtil.isImage(asset))
        		assets.add(asset);
        	if(child.hasChildren() && assets.size() <= bucketCount)
        		getAssetsFromParent(child, assets);
    	});
    	Collections.shuffle(assets);
		return assets;
	}
    
    private CarouselItem createCarouselItemFromImage(Asset image) {
    	return new CarouselItem(image.getPath(), image.getMetadataValue(DamConstants.DC_TITLE), image.getMetadataValue(DamConstants.DC_DESCRIPTION), null, null);
    }

}
