package org.subra.aem.foundation.restclient.helpers;

import org.apache.http.client.HttpClient;

/**
 * Interface for getting the common http client. The http client is the Apache HttpClient
 * @author raghava
 *
 */
public interface SubraHttpClient {

    HttpClient getHttpClient();
}
