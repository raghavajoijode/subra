package org.subra.aem.foundation.example.workflow.services;

import java.util.List;

import javax.jcr.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.subra.aem.foundation.example.workflow.beans.WorkFlowReportBean;

/**
 * @author RA324710
 *
 */

public interface WorkFlowReportService {

	List<WorkFlowReportBean> getReportList();

	JSONArray queryBuilder(Session session, String status, String startDate, String endDate) throws JSONException;

}
