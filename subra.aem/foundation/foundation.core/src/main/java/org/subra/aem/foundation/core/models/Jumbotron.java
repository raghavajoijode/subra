package org.subra.aem.foundation.core.models;

import org.subra.aem.foundation.core.internal.components.base.SubraComponent;

/**
 * Sling model mapped with jumbotron component - Resource type
 * "foundation/components/content/jumbotron"
 * 
 * @author Raghava Joijode
 *
 */
public interface Jumbotron extends SubraComponent {

	default String getHeading() {
		throw new UnsupportedOperationException();
	}

	default String getText() {
		throw new UnsupportedOperationException();
	}

}
