/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.subra.aem.foundation.core.models;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.foundation.core.dtos.product.ProductDto;
import org.subra.aem.foundation.core.services.ProductService;

@Model(adaptables = { Resource.class })
public class ProductModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductModel.class);

	@OSGiService
	private ProductService productService;
	private ProductDto product;

	@PostConstruct
	protected void init() {
		LOGGER.debug("{} init..", getClass().getSimpleName());
		List<String> p = Collections.emptyList();
		product = productService.getProducts(p);
	}

	public ProductDto getProduct() {
		return product;
	}
}
