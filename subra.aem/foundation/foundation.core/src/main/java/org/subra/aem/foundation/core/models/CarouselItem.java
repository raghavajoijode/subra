package org.subra.aem.foundation.core.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Model(adaptables = SlingHttpServletRequest.class)
public class CarouselItem {

	@Inject @Optional
	private String image;
	@Inject @Optional
	private String heading;
	@Inject @Optional
	private String subHeading;
	@Inject @Optional
	private String buttonText;
	@Inject @Optional
	private String buttonLink;

	@Override
	public String toString() {
		return "[Carousel : " + super.toString() + "]";
	}
}