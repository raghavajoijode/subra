package org.subra.aem.foundation.core.helpers.impl;

import org.apache.sling.i18n.ResourceBundleProvider;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.helpers.ActivatonHelper;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.commons.jcr.utils.SubraI18NUtil;
import org.subra.aem.commons.jcr.utils.SubraModeUtils;

@Component(service = ActivatonHelper.class, immediate = true)
@ServiceDescription("Subra - Activator Helper")
public class ActivatonHelperImpl extends SubraGenericServiceImpl implements ActivatonHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActivatonHelperImpl.class);

	@Reference
	private SlingSettingsService slingSettings;

	@Reference
	private ResourceBundleProvider resourceBundleProvider;

	@Activate
	private void activate() {
		try {
			SubraModeUtils.configure(slingSettings);
			SubraI18NUtil.configure(resourceBundleProvider);
		} catch (ConfigurationException e) {
			sendExceptionMessage(getClass().getSimpleName(), e.getMessage());
			LOGGER.error("ConfigurationException occured");
		}
	}

	@Deactivate
	private void deActivate() {
		SubraCommonHelper.clearLogFiles(slingSettings.getSlingHomePath());
	}
}