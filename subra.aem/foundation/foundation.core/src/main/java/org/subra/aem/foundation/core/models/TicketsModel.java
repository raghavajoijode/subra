package org.subra.aem.foundation.core.models;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.helpers.RequestParser;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.foundation.core.dtos.BreadcrumbItem;
import org.subra.aem.foundation.core.services.TicketService;
import org.subra.common.api.dtos.TicketDto;

@Model(adaptables = { SlingHttpServletRequest.class })
public class TicketsModel {

	private static final String ALL_TICKETS_KEY = "all-tickets";

	private static final Logger LOGGER = LoggerFactory.getLogger(TicketsModel.class);

	@Self
	private SlingHttpServletRequest request;

	@OSGiService
	private TicketService ticketService;

	private boolean isTicketDetailRequest;
	private TicketDto ticket;
	private List<TicketDto> tickets;

	@ValueMapValue
	@Default(values = "Tickets")
	private String ticketsBreadCrumbLabel;

	@ValueMapValue
	@Default(values = "..\\")
	private String ticketsBreadCrumbLink;

	private static final Pattern TICKET_NUMBER_PATTERN = Pattern.compile("^([A-Z]{3}\\d{3}\\d+)$");

	@PostConstruct
	protected void init() {
		LOGGER.debug("{} init..", getClass().getSimpleName());
		Optional<String> optionalTicketNumber = RequestParser.getSelector(request, TICKET_NUMBER_PATTERN);
		isTicketDetailRequest = optionalTicketNumber.isPresent();
		if (isTicketDetailRequest)
			ticket = SubraCommonHelper.getCacheData(request, optionalTicketNumber.get(),
					() -> ticketService.getTicket(optionalTicketNumber.get()));
		else
			tickets = SubraCommonHelper.getCacheData(request, ALL_TICKETS_KEY, () -> ticketService.getTickets());
	}

	public List<TicketDto> getTickets() {
		return tickets;
	}

	public TicketDto getTicket() {
		return ticket;
	}

	public List<BreadcrumbItem> getBreadcrumbs() {
		List<BreadcrumbItem> breadcrumbs = new LinkedList<>();
		BreadcrumbItem ticketsCrumb = new BreadcrumbItem("Label", "/", true);
		ticketsCrumb.setLabel(ticketsBreadCrumbLabel);
		ticketsCrumb.setLink(ticketsBreadCrumbLink);
		breadcrumbs.add(ticketsCrumb);
		if (isTicketDetailRequest) {
			BreadcrumbItem ticketCrumb = new BreadcrumbItem("Label", "/", true);
			ticketCrumb.setLabel(ticket.getTicketNumber());
			breadcrumbs.add(ticketCrumb);
		}
		return breadcrumbs;
	}

	public boolean isTicketList() {
		return !isTicketDetailRequest;
	}
}
