package org.subra.aem.foundation.core.helpers.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.http.HttpHeaders;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraHttpType;
import org.subra.aem.commons.exceptions.SubraApiException;
import org.subra.aem.foundation.core.helpers.SubraGenericService;
import org.subra.aem.foundation.restclient.services.SubraRestClientService;
import org.subra.aem.mailer.services.SubraTemplatedEmailService;
import org.subra.aem.mailer.utils.EmailType;
import org.subra.common.api.dtos.Response;

@Component(service = SubraGenericService.class, immediate = true)
@ServiceDescription("Subra - Generic Service")
public class SubraGenericServiceImpl implements SubraGenericService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubraGenericServiceImpl.class);
	private static final String ADMIN_RECIPENT_EMAIL = "raghava.joijode@gmail.com";
	private static final String ADMIN_RECIPENT_NAME = "Admin";
	private static final String DEFAULT_SENDER_NAME = "Subra Group";

	@Reference
	SubraTemplatedEmailService templatedEmailService;

	@Reference
	SubraRestClientService subraRestClient;

	@Activate
	private void init() {
		this.sendElectronicMessage("Alert: Services Activated!",
				"This is to let you know that all the services are activated");
	}

	@Override
	public void sendExceptionMessage(String className, String message) {
		boolean status = templatedEmailService.email(EmailType.EXCEPTION, "[EXCEPTION] :" + className,
				ADMIN_RECIPENT_NAME, DEFAULT_SENDER_NAME, null, Collections.singletonMap("exception", message),
				ADMIN_RECIPENT_EMAIL);
		if (status) {
			LOGGER.info("Sent Email with message {}", message);
		} else {
			LOGGER.error("Error sending Email with message {}", message);
		}
	}

	@Override
	public void sendElectronicMessage(String subject, String message) {
		boolean status = templatedEmailService.email(EmailType.GENERIC, subject, ADMIN_RECIPENT_NAME,
				DEFAULT_SENDER_NAME, null, Collections.singletonMap("message", message), ADMIN_RECIPENT_EMAIL);
		if (status) {
			LOGGER.info("Sent Email with message {}", message);
		} else {
			LOGGER.error("Error sending Email with message {}", message);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T callBackendService(final String endpointUrl, final String resource,
			final Map<String, String> requestHeaders, final Map<String, String> queryParams, final Object postObject,
			final SubraHttpType requestType) {
		final Map<String, String> headers = new HashMap<>();
		if (requestHeaders != null)
			headers.putAll(requestHeaders);

		headers.put(HttpHeaders.ACCEPT, "application/json");
		headers.put(HttpHeaders.CONTENT_TYPE, "application/json");
		Response response = null;
		try {
			if (requestType.equals(SubraHttpType.GET))
				response = subraRestClient.getData(endpointUrl, resource, headers, queryParams, Response.class);

			if (requestType.equals(SubraHttpType.POST))
				response = subraRestClient.postData(endpointUrl, resource, headers, queryParams, postObject,
						Response.class);

		} catch (SubraApiException e) {
			LOGGER.error("Error calling backend service", e);
		}
		return (T) Optional.ofNullable(response).map(Response::getBody).orElse(null);
	}

}
