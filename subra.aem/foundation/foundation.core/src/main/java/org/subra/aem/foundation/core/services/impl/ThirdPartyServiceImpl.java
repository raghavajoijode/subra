package org.subra.aem.foundation.core.services.impl;

import java.net.URI;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.subra.aem.commons.exceptions.SubraRuntimeException;
import org.subra.aem.foundation.core.services.ThirdPartyService;

@Component(immediate = true, service = ThirdPartyService.class)
public class ThirdPartyServiceImpl implements ThirdPartyService {

	@Override
	public String getJSONData(String uri) {
		return getClientResponse(uri);
	}

	private String getClientResponse(String clientUri) {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet getRequest = new HttpGet(URI.create(clientUri));
			getRequest.addHeader("accept", "application/json");
			String response;
			try (CloseableHttpResponse httpResponse = httpClient.execute(getRequest)) {
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					response = EntityUtils.toString(httpResponse.getEntity());
				} else {
					throw new SubraRuntimeException("Exception!! at ThirdPartyServiceImpl : HttpResponse error code : "
							+ httpResponse.getStatusLine().getStatusCode());
				}
			}
			JSONObject jsonObject = new JSONObject(response);
			return jsonObject.toString();
		} catch (Exception e) {
			throw new SubraRuntimeException("Exception!! at ThirdPartyServiceImpl : with HttpClient " + e.getMessage());
		}
	}

}
