package org.subra.aem.foundation.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.subra.aem.foundation.core.services.ThirdPartyService;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Simple Demo Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/tps" })
public class ThirdPartyServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = -7639144471855594170L;

	@Reference
	ThirdPartyService tps;

	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/json");
		String uri = req.getParameter("uri") != null ? req.getParameter("uri") : null;
		if (uri != null)
			resp.getWriter().print(tps.getJSONData(uri));
		else
			resp.getWriter().print("No_URI_PROVIDED");
	}
}
