package org.subra.aem.foundation.core.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraHttpType;
import org.subra.aem.ehcache.services.CacheService;
import org.subra.aem.foundation.core.helpers.impl.SubraGenericServiceImpl;
import org.subra.aem.foundation.core.services.UserService;
import org.subra.common.api.dtos.account.User;

@Component(immediate = true, service = UserService.class)
@Designate(ocd = UserServiceImpl.Config.class)
public class UserServiceImpl extends SubraGenericServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	private static final String USER_API_URL_TEMPLATE = "user.api.url.template";

	private String userApiUrlTemplate;

	private static final String USER_API_URL_VALUE = "http://localhost:8080/subra/api/v1/users";

	@ObjectClassDefinition(name = "Subra - UserServiceImpl configuration", description = "UserServiceImpl configuration")
	public @interface Config {
		@AttributeDefinition(name = USER_API_URL_TEMPLATE, description = "Template url for user service")
		String userApiUrlTemplate() default USER_API_URL_VALUE;
	}

	@Activate
	@Modified
	private void init(final Config config) {
		LOGGER.trace("Activate TicketsServiceImpl");
		userApiUrlTemplate = config.userApiUrlTemplate();
	}

	@Reference
	CacheService cacheManager;

	@Override
	public User getUser(String userid) {
		return callBackendService(userApiUrlTemplate, userid, null, null, null, SubraHttpType.GET);
	}

}
