package org.subra.aem.foundation.example.multicinfig.services;

/**
 * 
 * Configuration with Name, place, age;
 * 
 */
public interface MyTestSampleService {
	
	String getName();
	String getPlace();
	int getPIN();

}
