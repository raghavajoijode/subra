package org.subra.aem.foundation.core.dtos.product;

public class ProductDto {
	private int id;
	private String name;
	private String description;
	private String availability;
	private ProductPriceDto pricing;
	private ProductShippingDto shipping;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public ProductPriceDto getPricing() {
		return pricing;
	}

	public void setPricing(ProductPriceDto pricing) {
		this.pricing = pricing;
	}

	public ProductShippingDto getShipping() {
		return shipping;
	}

	public void setShipping(ProductShippingDto shipping) {
		this.shipping = shipping;
	}

	public class ProductPriceDto {
		private int mrpPrice;
		private int sellingPrice;
		private String currency;

		public ProductPriceDto() {
			super();
		}

		public int getMrpPrice() {
			return mrpPrice;
		}

		public void setMrpPrice(int mrpPrice) {
			this.mrpPrice = mrpPrice;
		}

		public int getSellingPrice() {
			return sellingPrice;
		}

		public void setSellingPrice(int sellingPrice) {
			this.sellingPrice = sellingPrice;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

	}

	public class ProductShippingDto {
		private boolean shippable;
		private int shippingPrice;
		private int maxShippingDays;

		public ProductShippingDto() {
			super();
		}

		public boolean isShippable() {
			return shippable;
		}

		public void setShippable(boolean isShippable) {
			this.shippable = isShippable;
		}

		public int getShippingPrice() {
			return shippingPrice;
		}

		public void setShippingPrice(int shippingPrice) {
			this.shippingPrice = shippingPrice;
		}

		public int getMaxShippingDays() {
			return maxShippingDays;
		}

		public void setMaxShippingDays(int maxShippingDays) {
			this.maxShippingDays = maxShippingDays;
		}

	}

}