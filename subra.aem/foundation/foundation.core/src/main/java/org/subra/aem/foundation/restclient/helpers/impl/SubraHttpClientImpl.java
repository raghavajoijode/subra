package org.subra.aem.foundation.restclient.helpers.impl;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.foundation.restclient.helpers.SubraHttpClient;

@Component(service = SubraHttpClient.class, immediate = true)
@ServiceDescription("Subra - Http Client")
@Designate(ocd = SubraHttpClientImpl.Config.class)
public class SubraHttpClientImpl implements SubraHttpClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubraHttpClientImpl.class);
	private static final String SUBSERVICE_NAME = "subservice.name";
	private static final String HTTP_MAX_CONNECTIONS = "http.max.connections";
	private static final String HTTP_MAX_CONNECTIONS_PER_ROUTE = "http.max.connections.per.route";
	private static final String HTTP_TIMEOUT_REQUEST = "http.timeout.request.";
	private static final String HTTP_TIMEOUT_SOCKET = "http.timeout.socket";
	private static final String HTTP_TIMEOUT_CONNECT = "http.timeout.connect";

	private static String subserviceName;
	private static int maxHttpConnections;
	private static int maxHttpConnectionsPerRoute;
	private static int httpConnectRequestTimeout;
	private static int httpSocketTimeout;
	private static int httpConnectTimeout;

	private static final String SUBSERVICE_NAME_VALUE = "httpClient";
	private static final int HTTP_MAX_CONNECTIONS_VALUE = 1000;
	private static final int HTTP_MAX_CONNECTIONS_PER_ROUTE_VALUE = 500;
	private static final int HTTP_TIMEOUT_CONNECT_VALUE = 1000;
	private static final int HTTP_TIMEOUT_SOCKET_VALUE = 10000;

	private CloseableHttpClient httpClient;

	@Override
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 * Initialize the http client. If already exists, deallocate it first.
	 *
	 * @throws Exception
	 */
	protected synchronized void initHttpClient() {

		if (httpClient != null) {
			closeHttpClient();
		}

		HttpClientConnectionManager connectionManager = getConnectionManager();
		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SubraHttpClientImpl.httpSocketTimeout)
				.setConnectionRequestTimeout(SubraHttpClientImpl.httpConnectRequestTimeout)
				.setConnectTimeout(SubraHttpClientImpl.httpConnectTimeout).build();
		httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.setConnectionManager(connectionManager).build();

	}

	/**
	 * Close the http client
	 */
	public final synchronized void closeHttpClient() {

		LOGGER.info("Closing HttpClient");

		// method call added for junit testing
		if (getHttpClient() == null) {
			LOGGER.info("HttpClient is null");
			return;
		}
		try {
			((CloseableHttpClient) getHttpClient()).close();
		} catch (IOException ioe) {
			LOGGER.error("Error closing http client", ioe);
		} finally {
			httpClient = null;
		}
	}

	/**
	 * Get/create the connection manager for this http client.
	 *
	 * @return
	 * @throws Exception
	 */
	protected HttpClientConnectionManager getConnectionManager() {
		PoolingHttpClientConnectionManager httpClientConnectionManager = new PoolingHttpClientConnectionManager();
		httpClientConnectionManager.setMaxTotal(SubraHttpClientImpl.maxHttpConnections);
		httpClientConnectionManager.setDefaultMaxPerRoute(SubraHttpClientImpl.maxHttpConnectionsPerRoute);

		return httpClientConnectionManager;
	}

	public static String getSubserviceName() {
		return subserviceName;
	}

	public static void setSubserviceName(final String subserviceName) {
		SubraHttpClientImpl.subserviceName = subserviceName;
	}

	public static int getMaxHttpConnections() {
		return maxHttpConnections;
	}

	public static void setMaxHttpConnections(final int maxHttpConnections) {
		SubraHttpClientImpl.maxHttpConnections = maxHttpConnections;
	}

	public static int getMaxHttpConnectionsPerRoute() {
		return maxHttpConnectionsPerRoute;
	}

	public static void setMaxHttpConnectionsPerRoute(final int maxHttpConnectionsPerRoute) {
		SubraHttpClientImpl.maxHttpConnectionsPerRoute = maxHttpConnectionsPerRoute;
	}

	public static int getHttpConnectRequestTimeout() {
		return httpConnectRequestTimeout;
	}

	public static void setHttpConnectRequestTimeout(final int httpConnectRequestTimeout) {
		SubraHttpClientImpl.httpConnectRequestTimeout = httpConnectRequestTimeout;
	}

	public static int getHttpSocketTimeout() {
		return httpSocketTimeout;
	}

	public static void setHttpSocketTimeout(final int httpSocketTimeout) {
		SubraHttpClientImpl.httpSocketTimeout = httpSocketTimeout;
	}

	public static int getHttpConnectTimeout() {
		return httpConnectTimeout;
	}

	public static void setHttpConnectTimeout(final int httpConnectTimeout) {
		SubraHttpClientImpl.httpConnectTimeout = httpConnectTimeout;
	}

	@Activate
	@Modified
	public void activate(final Config config) {

		setMaxHttpConnections(config.maxHttpConnections());
		setMaxHttpConnectionsPerRoute(config.maxHttpConnectionsPerRoute());
		setHttpConnectRequestTimeout(config.httpConnectRequestTimeout());
		setHttpSocketTimeout(config.httpSocketTimeout());
		setHttpConnectTimeout(config.httpConnectTimeout());
		setSubserviceName(config.subserviceName());

		LOGGER.info(
				"Http configuration: MaxConn [{}] :: MaxConnPerRoute: [{}] :: ReqTO: [{}] :: SocketTO: [{}] :: ConnectTO: [{}]",
				getMaxHttpConnections(), getMaxHttpConnectionsPerRoute(), getHttpConnectRequestTimeout(),
				getHttpSocketTimeout(), getHttpConnectTimeout());
		LOGGER.info("Initializing HttpClient");
		initHttpClient();
	}

	@Deactivate
	public void deactivate() {
		// close the http client
		LOGGER.info("Deactivating the HttpClient");
		closeHttpClient();
	}

	@ObjectClassDefinition(name = "Subra - Http Client", description = "Service for http configuration")
	public @interface Config {

		@AttributeDefinition(name = SUBSERVICE_NAME, description = "Subservice name for query service")
		String subserviceName() default SUBSERVICE_NAME_VALUE;

		@AttributeDefinition(name = HTTP_MAX_CONNECTIONS, description = "Maximum http connections for the client")
		int maxHttpConnections() default HTTP_MAX_CONNECTIONS_VALUE;

		@AttributeDefinition(name = HTTP_MAX_CONNECTIONS_PER_ROUTE, description = "Maximum http connections per route for the client")
		int maxHttpConnectionsPerRoute() default HTTP_MAX_CONNECTIONS_PER_ROUTE_VALUE;

		@AttributeDefinition(name = HTTP_TIMEOUT_REQUEST, description = "Request connect timeout (in milliseconds) for requesting connection from connection manager for the client")
		int httpConnectRequestTimeout() default HTTP_TIMEOUT_CONNECT_VALUE;

		@AttributeDefinition(name = HTTP_TIMEOUT_SOCKET, description = "Socket (receive) timeout (in milliseconds) for the client")
		int httpSocketTimeout() default HTTP_TIMEOUT_SOCKET_VALUE;

		@AttributeDefinition(name = HTTP_TIMEOUT_CONNECT, description = "Connect  timeout (in milliseconds) for estabishing a connection for the client")
		int httpConnectTimeout() default HTTP_TIMEOUT_CONNECT_VALUE;
	}
}
