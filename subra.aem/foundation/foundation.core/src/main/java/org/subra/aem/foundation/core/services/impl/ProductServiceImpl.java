package org.subra.aem.foundation.core.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.utils.SubraDateTimeUtil;
import org.subra.aem.ehcache.helpers.CacheHelper;
import org.subra.aem.ehcache.services.CacheService;
import org.subra.aem.foundation.core.dtos.product.ProductDto;
import org.subra.aem.foundation.core.helpers.impl.SubraGenericServiceImpl;
import org.subra.aem.foundation.core.services.ProductService;
import org.subra.aem.foundation.restclient.services.SubraRestClientService;

@Component(immediate = true, service = ProductService.class)
public class ProductServiceImpl extends SubraGenericServiceImpl implements ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Reference
	CacheService cacheManager;

	@Reference
	SubraRestClientService subraRestClient;

	@Override
	public ProductDto getProducts(List<String> productNumbers) {
		ProductDto product = null;
		String cacheKey = String.format("%s-%s-%s", "product-service",
				productNumbers.stream().collect(Collectors.joining("-")), SubraDateTimeUtil.getCurrentHour());

		CacheHelper<ProductDto> cache = cacheManager.getInstanceCache(getClass().getName(), "in");
		CacheHelper<ProductDto> failsafeCache = cacheManager.getInstanceCache(getClass().getName().concat("-failsafe"),
				"in");
		ProductDto cachedData = cache.get(cacheKey);
		if (cachedData != null) {
			LOGGER.debug("getting cached data with Key {}", cacheKey);
			return cachedData;
		}
		try {
			LOGGER.debug("Calling rest service");
			product = subraRestClient.getDataFromResource("single-product.json", ProductDto.class);
			cache.put(cacheKey, product);
			failsafeCache.put(cacheKey, product);
		} catch (Exception e) {
			sendExceptionMessage(getClass().getName(), e.getMessage());
			LOGGER.error("Exception occured in {}, {}", getClass().getSimpleName(), e.getMessage());
			product = failsafeCache.get(cacheKey);
		}

		return product;
	}

}
