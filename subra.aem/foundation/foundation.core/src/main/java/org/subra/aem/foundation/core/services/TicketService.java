package org.subra.aem.foundation.core.services;

import java.util.List;

import org.subra.common.api.dtos.TicketDto;

public interface TicketService {
	List<TicketDto> getTickets();
	TicketDto getTicket(String number);
}
