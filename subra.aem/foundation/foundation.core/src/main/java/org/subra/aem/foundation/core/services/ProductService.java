package org.subra.aem.foundation.core.services;

import java.util.List;

import org.subra.aem.foundation.core.dtos.product.ProductDto;

public interface ProductService {
	ProductDto getProducts(List<String> productNumbers);
}
