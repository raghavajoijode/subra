package org.subra.aem.foundation.core.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.subra.aem.foundation.core.dtos.BreadcrumbItem;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Style;

@Model(adaptables = SlingHttpServletRequest.class)
public class BreadcrumbModel {

    protected static final boolean PROP_SHOW_HIDDEN_DEFAULT = false;
    protected static final boolean PROP_HIDE_CURRENT_DEFAULT = false;
    protected static final int PROP_START_LEVEL_DEFAULT = 2;
    protected static final String PN_SHOW_HIDDEN = "showHidden";
    protected static final String PN_HIDE_CURRENT = "hideCurrent";
    protected static final String PN_START_LEVEL = "startLevel";

    @ScriptVariable
    private ValueMap properties;

    @ScriptVariable
    private Style currentStyle;

    @ScriptVariable
    private Page currentPage;

    @Self
    private SlingHttpServletRequest request;

    private boolean showHidden;
    private boolean hideCurrent;
    private int startLevel;
    private List<BreadcrumbItem> items;

    @PostConstruct
    private void initModel() {
        startLevel = properties.get(PN_START_LEVEL, currentStyle.get(PN_START_LEVEL, PROP_START_LEVEL_DEFAULT));
        showHidden = properties.get(PN_SHOW_HIDDEN, currentStyle.get(PN_SHOW_HIDDEN, PROP_SHOW_HIDDEN_DEFAULT));
        hideCurrent = properties.get(PN_HIDE_CURRENT, currentStyle.get(PN_HIDE_CURRENT, PROP_HIDE_CURRENT_DEFAULT));
        if (CollectionUtils.isEmpty(items)) {
            items = createItems();
        }
        //TODO : Take list items from Authoring 
    }

    public Collection<BreadcrumbItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    private List<BreadcrumbItem> createItems() {
        List<BreadcrumbItem> bcItems = new ArrayList<>();
        int currentLevel = currentPage.getDepth();
        while (startLevel < currentLevel) {
            Page page = currentPage.getAbsoluteParent(startLevel);
            if (page != null) {
                boolean isActivePage = page.equals(currentPage);
                if (isActivePage && hideCurrent) {
                    break;
                }
                if (checkIfNotHidden(page)) {//page, isActivePage, request, currentLevel, Collections.emptyList()
                	BreadcrumbItem navigationItem = new BreadcrumbItem(getTitle(page), getURL(page) + ".html", isActivePage);
                    bcItems.add(navigationItem);
                }
            }
            startLevel++;
        }
        return bcItems;
    }

    private boolean checkIfNotHidden(Page page) {
        return !page.isHideInNav() || showHidden;
    }
    
    private String getURL(Page page) {
    	String vanityURL = page.getVanityUrl();
        return StringUtils.isEmpty(vanityURL) ? (request.getContextPath() + page.getPath() + ".html"): (request.getContextPath() + vanityURL);
    }

    private String getTitle(Page page) {
        String title = page.getNavigationTitle();
        if (title == null) {
            title = page.getPageTitle();
        }
        if (title == null) {
            title = page.getTitle();
        }
        if (title == null) {
            title = page.getName();
        }
        return title;
    }
}
