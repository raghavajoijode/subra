package org.subra.aem.foundation.core.services;

public interface ThirdPartyService {
	String getJSONData(String uri);
}
