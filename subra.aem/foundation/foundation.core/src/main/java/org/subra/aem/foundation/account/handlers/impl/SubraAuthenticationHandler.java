package org.subra.aem.foundation.account.handlers.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.auth.Authenticator;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.auth.core.AuthConstants;
import org.apache.sling.auth.core.AuthUtil;
import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceRanking;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraUserMapperService;
import org.subra.aem.commons.helpers.CookieHelper;
import org.subra.aem.commons.jcr.utils.SubraModeUtils;
import org.subra.aem.commons.utils.SubraStringUtils;
import org.subra.aem.foundation.account.services.SubraUserService;
import org.subra.common.api.utils.SubraJwtTokenUtil;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;

@Component(service = AuthenticationHandler.class, immediate = true)
@ServiceRanking(10)
@ServiceDescription("Subra Authentication Handler")
@Designate(ocd = SubraAuthenticationHandler.Config.class)

public class SubraAuthenticationHandler implements AuthenticationHandler, AuthenticationFeedbackHandler {

	private static final String REQUEST_METHOD = "POST";
	private static final String U_NAME = "j_username";
	private static final String J_PASS = "j_password";
	static final String REQUEST_URL_SUFFIX = "/j_security_check";
	private static final String PAR_LOOP_PROTECT = "$$login$$";
	private static final String SUBRA_AUTH_TYPE = "SUBRA_AUTH";

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Reference
	private SubraUserService userService;

	@Reference
	private CryptoSupport cryptoSupport;

	private static final Logger LOGGER = LoggerFactory.getLogger(SubraAuthenticationHandler.class);

	private Config config;
	private ResourceResolver resourceResolver;
	private String webUserName;
	private String webUserPassword;
	private String jwtSecretKey;
	private long expiryInMs;

	@Activate
	private void activate(final Config c) {
		this.config = c;
		webUserName = getDecryptedValue(config.web_user_name());
		webUserPassword = getDecryptedValue(config.web_user_password());
		jwtSecretKey = getDecryptedValue(config.jwt_secret_key());
		expiryInMs = 3600000;
		SubraJwtTokenUtil.configure(expiryInMs, jwtSecretKey);
		try {
			this.resourceResolver = resourceResolverFactory.getServiceResourceResolver(Collections
					.singletonMap(ResourceResolverFactory.SUBSERVICE, SubraUserMapperService.ADMIN_SERVICE.value()));
		} catch (LoginException e) {
			LOGGER.error("Logging exception occured when obtaining resourceResolver");
		}
	}

	@Override
	public AuthenticationInfo extractCredentials(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.trace("Authenticating for {}...", request.getRequestURI());
		final boolean isLogin = REQUEST_METHOD.equals(request.getMethod())
				&& request.getRequestURI().endsWith(REQUEST_URL_SUFFIX);
		boolean isValid = false;
		if (isLogin) {
			final AuthenticationInfo authInfo = this.extractRequestParameterAuthentication(request);
			if (!AuthUtil.isValidateRequest(request)) {
				AuthUtil.setLoginResourceAttribute(request, request.getContextPath());
			}
			final String token = userService.authenticateUser(authInfo.getUser(),
					String.valueOf(authInfo.getPassword()));
			if (StringUtils.equalsIgnoreCase(SubraJwtTokenUtil.extractUserName(token), authInfo.getUser())) {
				request.setAttribute("tokenKey", token);
				isValid = true;
			} else {
				dropCredentials(request, response);
			}
		} else {
			// we aren't logging in, but validate the authorization
			isValid = isAuthorized(request, response);
			LOGGER.trace("Validating existing token: {}", isValid ? "valid" : "");
		}
		return mappingTrustedAuthInfo(isValid, isLogin);
	}

	private AuthenticationInfo extractRequestParameterAuthentication(final HttpServletRequest request) {
		final String user = request.getParameter(U_NAME);
		final String pwd = request.getParameter(J_PASS);
		return SubraStringUtils.isNoneBlank(user, pwd)
				? new AuthenticationInfo(SUBRA_AUTH_TYPE, user, pwd.toCharArray())
				: null;
	}

	protected AuthenticationInfo mappingTrustedAuthInfo(final boolean isValid, final boolean isLogin) {
		LOGGER.trace("Mapping trusted auth info, isValid: {}, isLogin: {}", isValid, isLogin);
		final SimpleCredentials repositoryCredentials = new SimpleCredentials(webUserName,
				webUserPassword.toCharArray());
		// return only this will go to authenticationFailed
		final AuthenticationInfo aInfo = new AuthenticationInfo(SUBRA_AUTH_TYPE, repositoryCredentials.getUserID());
		if (isValid) {
			// Add the credentials obj to the AuthenticationInfo obj - so it will go to
			// authenticationSucceded
			aInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_CREDENTIALS, repositoryCredentials);
			if (isLogin) {
				// Marker property in the AuthenticationInfo object indicating a first
				// authentication considered to be a login
				aInfo.put(AuthConstants.AUTH_INFO_LOGIN, new Object());
			}
			return aInfo;
		}
		return isLogin ? aInfo : null;
	}

	protected boolean isAuthorized(final HttpServletRequest request, final HttpServletResponse response) {
		boolean isAuthorized = false;
		if (request == null) {
			return isAuthorized;
		}
		final String subraAuthKey = CookieHelper.getAuthKey(request);
		final String uri = request.getRequestURI();
		if (StringUtils.isBlank(subraAuthKey) || uri == null) {
			return isAuthorized;
		}
		try {
			isAuthorized = !SubraJwtTokenUtil.isTokenExpired(subraAuthKey);
		} catch (Exception e) {
			LOGGER.error("Exception occured authorizing ", e);
			dropCredentials(request, response);
		}
		return isAuthorized;
	}

	@Override
	public void dropCredentials(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.trace("dropCredentials-start {}", request.getPathInfo());
		CookieHelper.deleteAccountId(response);
		CookieHelper.deleteAuthKey(response, request.getServerName());
	}

	@Override
	public boolean requestCredentials(HttpServletRequest request, HttpServletResponse response) throws IOException {
		LOGGER.trace("requestCredentials-start {}", request.getPathInfo());
		String reason = getReason(request, FAILURE_REASON);
		String reasonCode = getReason(request, FAILURE_REASON_CODE);
		try {
			String path = rewrite(config.login_page_path());
			LinkedHashMap<String, String> params = new LinkedHashMap<>();
			params.put(Authenticator.LOGIN_RESOURCE, path);
			params.put(PAR_LOOP_PROTECT, PAR_LOOP_PROTECT);
			// append indication of previous login failure
			params.put(FAILURE_REASON, reason);
			params.put(FAILURE_REASON_CODE, reasonCode);
			AuthUtil.sendRedirect(request, response, path, params);
		} catch (IOException e) {
			LOGGER.error("[IOException] Failed to redirect to the login / change password form [{}]", e);
		}
		final String requestLogin = request.getParameter(REQUEST_LOGIN_PARAMETER);
		return requestLogin != null && !SUBRA_AUTH_TYPE.equals(requestLogin);
	}

	private String rewrite(final String path) {
		String loginPagePath = config.default_login_page_path();
		if (resourceResolver.getResource(StringUtils.removeEndIgnoreCase(path, ".HTML")) != null
				&& SubraModeUtils.isPublish()) {
			loginPagePath = StringUtils.removeEndIgnoreCase(path, ".HTML");
		}
		return StringUtils.endsWith(loginPagePath, ".html") ? loginPagePath : loginPagePath.concat(".html");
	}

	private static String getReason(HttpServletRequest request, String parameter) {
		Object reason = request.getAttribute(parameter);
		if (null == reason) {
			reason = request.getParameter(parameter);
		}
		if (null == reason) {
			reason = FAILURE_REASON_CODES.UNKNOWN;
		}
		return (reason instanceof Enum) ? ((Enum<?>) reason).name().toLowerCase() : reason.toString();
	}

	@Override
	public void authenticationFailed(HttpServletRequest request, HttpServletResponse response,
			AuthenticationInfo authInfo) {
		request.setAttribute("tokenKey", StringUtils.EMPTY);
		LOGGER.trace("authenticationFailed");
		AuthUtil.sendInvalid(request, response); // 403 Forbidden
	}

	@Override
	public boolean authenticationSucceeded(HttpServletRequest request, HttpServletResponse response,
			AuthenticationInfo authInfo) {
		// if we are doing a redirect, then redirect
		if (REQUEST_METHOD.equals(request.getMethod()) && request.getRequestURI().endsWith(REQUEST_URL_SUFFIX)
				&& authInfo != null && webUserName.equals(authInfo.getUser())) {
			final String tokenKey = (String) request.getAttribute("tokenKey");
			final String acid = SubraJwtTokenUtil.extractAccid(tokenKey);
			if (SubraStringUtils.isNoneBlank(tokenKey, acid)) {
				CookieHelper.setAccountId(response, acid, false);
				CookieHelper.setAuthKey(response, request.getServerName(), tokenKey, false);
			} else {
				dropCredentials(request, response);
			}
		}
		return false;
	}

	protected String getDecryptedValue(final String value) {
		String decryptedValue = null;
		try {
			decryptedValue = cryptoSupport.isProtected(value) ? cryptoSupport.unprotect(value) : value;
		} catch (CryptoException ce) {
			LOGGER.error("Exception occurred decrypting value", ce);
		}
		return decryptedValue;
	}

	@ObjectClassDefinition(name = "Subra AuthenticationHandler Interface", description = "Two Factor Authentication Handler Interface Configuration")
	public @interface Config {

		@AttributeDefinition(name = "Path", description = "Repository path for which this authentication handler should be used by Sling. If this is empty, the authentication handler will be disabled. (path)")
		String path() default "/content/subra";

		@AttributeDefinition(name = "Path", description = "Repository path for which this authentication handler should be used by Sling. If this is empty, the authentication handler will be disabled. (path)")
		String authtype() default SUBRA_AUTH_TYPE;

		@AttributeDefinition(name = "Custum Login Page", description = "If no mappings are defined, nor no mapping matches the request, this is the default login page being redirected to. This can be overridden in the content page configuration")
		String login_page_path() default "/libs/granite/core/content/login";

		@AttributeDefinition(name = "Web User Name", description = "")
		String web_user_name() default "{d12c3a8fef5c00b348a3bd4052a88c856e3b9ed457ba9f50827e5cdd5a5d0b6a}";

		@AttributeDefinition(name = "Web User Password", description = "")
		String web_user_password() default "{b0cf2c15da0a2a02c48db1c4abe99a737f7b16919fd5c9c91b545facd7016f2d}";

		@AttributeDefinition(name = "Default Login Page", description = "If no mappings are defined, nor no mapping matches the request, this is the default login page being redirected to. This can be overridden in the content page configuration")
		String default_login_page_path() default "/libs/granite/core/content/login";

		@AttributeDefinition(name = "Default Login Page", description = "If no mappings are defined, nor no mapping matches the request, this is the default login page being redirected to. This can be overridden in the content page configuration")
		String jwt_secret_key() default "{a27f7a0bba35eaa3117fdb4e9c114d5f6d1213d00be375013f6b805d3c64cb285dab8799c9c2ab74081555b6ad2c946e}";
	}
}