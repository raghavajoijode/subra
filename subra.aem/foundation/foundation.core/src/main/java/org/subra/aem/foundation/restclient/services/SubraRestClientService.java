package org.subra.aem.foundation.restclient.services;

import java.io.OutputStream;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.http.Header;
import org.subra.aem.commons.exceptions.SubraApiException;
import org.subra.aem.foundation.restclient.dtos.SubraRestClientResponseDto;


public interface SubraRestClientService {
	
	/**
     * Get data from an REST service.
     *
     * @param endpointUrl - target uri of service
     * @param resource - resource to get
     * @param requestHeaders - Map of any required request headers
     * @param queryParams - MultiValueMap for any query parameters required (e.g. for search)
     * @param clazz - target class for response. String.class is not de-serialized. Response is de-serialized into other classes.
     *
     * @author 00922659
     */
    <T> T getDataFromResource(final String resourceName, final Class<T> clazz) throws SubraApiException;

    
    /**
     * Get data from an REST service.
     *
     * @param endpointUrl - target uri of service
     * @param resource - resource to get
     * @param requestHeaders - Map of any required request headers
     * @param queryParams - MultiValueMap for any query parameters required (e.g. for search)
     * @param clazz - target class for response. String.class is not de-serialized. Response is de-serialized into other classes.
     *
     * @author 00922659
     */
    <T> T getData(final String endpointUrl, final String resource, final Map<String, String> requestHeaders, final Map<String,String> queryParams, final Class<T> clazz) throws SubraApiException;

    /**
     * Get data from an REST service and return the SubraRestClientResponseDto.
     *
     * @param endpointUrl - target uri of service
     * @param resource - resource to get
     * @param requestHeaders - Map of any required request headers
     * @param queryParams - MultiValueMap for any query parameters required (e.g. for search)
     * @param clazz - target class for response. String.class is not de-serialized. Response is de-serialized into other classes.
     *
     * @author 00922659
     */
    <T> SubraRestClientResponseDto<T> getDataAndGetResponse(String endpointUrl, String resource, Map<String,String> requestHeaders, Map<String,String> queryParams, Class<T> clazz) throws SubraApiException;
    
	/**
	 * Get data from an REST service.
	 *
	 * @param endpointUrl target uri of service to call
	 * @param resource resource to get
	 * @param requestHeaders Map of any required request headers
	 * @param queryParams MultiValueMap for any query parameters required (e.g. for search)
	 * @param clazz target class for response. String.class is not de-serialized. Response is de-serialized into other classes.
	 * @param retries number of retries to attempt
	 *
	 * @author tjansen
	 */
    <T> T getData(final String endpointUrl, final String resource, final Map<String,String> requestHeaders, final Map<String,String> queryParams, final Class<T> clazz, int retries) throws SubraApiException;

    /**
     * Get data from an REST service and return the SubraRestClientResponseDto.
     *
     * @param endpointUrl - target uri of service
     * @param resource - resource to get
     * @param requestHeaders - Map of any required request headers
     * @param queryParams - MultiValueMap for any query parameters required (e.g. for search)
     * @param clazz - target class for response. String.class is not de-serialized. Response is de-serialized into other classes.
     * @param retries - number of retries to attempt
     *
     * @author 00922659
     */
    <T> SubraRestClientResponseDto<T> getDataAndGetResponse(String endpointUrl, String resource, Map<String,String> requestHeaders, Map<String,String> queryParams, Class<T> clazz, int retries) throws SubraApiException;

    
    /**
     * Post data with no response returned.  
     * @param endpointUrl
     * @param resource
     * @param requestHeaders
     * @param queryParams
     * @param postObject
     * @throws Exception
     */
    void postData(String endpointUrl, String resource, Map<String, String> requestHeaders, Map<String, String> queryParams,
            Object postObject) throws SubraApiException;
    
    /**
     * Post data with no response returned.  
     * @param endpointUrl
     * @param resource
     * @param requestHeaders
     * @param queryParams
     * @param postObject
     * @throws Exception
     */
    <T> T postData(String endpointUrl, String resource, Map<String, String> requestHeaders, Map<String, String> queryParams,
            Object postObject, Class<T> clazz) throws SubraApiException;

    /**
     * Post data and get the object in a wrapper DTO for access to other attributes from the response
     * 
     * @param endpointUrl
     * @param resource
     * @param requestHeaders
     * @param queryParams
     * @param postObject
     * @param clazz
     * @return
     * @throws Exception
     */
    <T> SubraRestClientResponseDto<T> postDataAndGetResponse(String endpointUrl, String resource, Map<String, String> requestHeaders,
            Map<String, String> queryParams, Object postObject, Class<T> clazz) throws SubraApiException;

    /**
     * Reads binary data from the given endpoint. As soon as the content type is known, the given consumer function is called. After
     * that, the content will be written into the given output stream.
     * 
     * @param endpointUrl
     *            - target uri of service
     * @param resource
     *            - resource to get
     * @param requestHeaders
     *            - Map of any required request headers
     * @param queryParams
     *            - MultiValueMap for any query parameters required (e.g. for search)
     * @param contentTypeConsumer
     *            a function that receives the content type of the request
     * @param target
     *            an OutputStream to write data to
     * @throws SubraApiException
     */
    void readBinaryData(String endpointUrl, String resource, Map<String, String> requestHeaders, Map<String, String> queryParams, Consumer<Header> contentTypeConsumer, Supplier<OutputStream> target) throws SubraApiException;
}
