package org.subra.aem.foundation.example.multicinfig.services;

public interface MyTestSampleMultiConfigService {
	String getMemberName();
	String getMemberPlace();
	int getMemberPIN();
}
