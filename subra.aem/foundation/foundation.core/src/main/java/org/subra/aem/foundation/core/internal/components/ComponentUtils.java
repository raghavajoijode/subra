package org.subra.aem.foundation.core.internal.components;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.drew.lang.annotations.NotNull;

public class ComponentUtils {

	/**
	 * Name of the separator character used between prefix and hash when generating
	 * an ID, e.g. image-5c7e0ef90d
	 */
	public static final String ID_SEPARATOR = "-";

	private ComponentUtils() {
		throw new IllegalArgumentException();
	}

	/**
	 * If the provided {@code path} identifies a {@link Page}, this method will
	 * generate the correct URL for the page. Otherwise the original {@code String}
	 * is returned.
	 *
	 * @param request     the current request, used to determine the server's
	 *                    context path
	 * @param pageManager the page manager
	 * @param path        the page path
	 * @return the URL of the page identified by the provided {@code path}, or the
	 *         original {@code path} if this doesn't identify a {@link Page}
	 */
	public static String getURL(SlingHttpServletRequest request, PageManager pageManager, String path) {
		Page page = pageManager.getPage(path);
		if (page != null) {
			return getURL(request, page);
		}
		return path;
	}

	/**
	 * Given a {@link Page}, this method returns the correct URL, taking into
	 * account that the provided {@code page} might provide a vanity URL.
	 *
	 * @param request the current request, used to determine the server's context
	 *                path
	 * @param page    the page
	 * @return the URL of the page identified by the provided {@code path}, or the
	 *         original {@code path} if this doesn't identify a {@link Page}
	 */
	@NotNull
	public static String getURL(@NotNull SlingHttpServletRequest request, @NotNull Page page) {
		String vanityURL = page.getVanityUrl();
		return StringUtils.isEmpty(vanityURL) ? (request.getContextPath() + page.getPath() + ".html")
				: (request.getContextPath() + vanityURL);
	}

	public enum HeadingTypes {

		H1("h1"), H2("h2"), H3("h3"), H4("h4"), H5("h5"), H6("h6");

		private String element;

		HeadingTypes(String element) {
			this.element = element;
		}

		public static HeadingTypes getHeading(String value) {
			for (HeadingTypes heading : values()) {
				if (StringUtils.equalsIgnoreCase(heading.element, value)) {
					return heading;
				}
			}
			return null;
		}

		public String getElement() {
			return element;
		}
	}

	/**
	 * Returns an ID based on the prefix, the ID_SEPARATOR and a hash of the path,
	 * e.g. image-5c7e0ef90d
	 *
	 * @param prefix the prefix for the ID
	 * @param path   the resource path
	 * @return the generated ID
	 */
	public static String generateId(String prefix, String path) {
		return StringUtils.join(prefix, ID_SEPARATOR, StringUtils.substring(DigestUtils.sha256Hex(path), 0, 10));
	}

}
