package org.subra.aem.foundation.example.sample.services;

/**
 * @author RA324710
 *
 */

public interface SocialMediaService {

	String getOAuthConsumerKey();

	String getOAuthConsumerSecret();

	String getOAuthAccessToken();

	String getOAuthaccessTokenSecret();

	String getFBCompanyID();

	String getFBOAuthAccessToken();

	String getYoutubeKey();

	String getYoutubeChannelName();

	String getLinkedInCompanyID();

	String getLinkedInAccessToken();
}