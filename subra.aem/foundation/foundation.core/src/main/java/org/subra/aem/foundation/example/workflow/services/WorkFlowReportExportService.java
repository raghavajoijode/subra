package org.subra.aem.foundation.example.workflow.services;

import java.util.List;

import org.subra.aem.foundation.example.workflow.beans.WorkFlowReportBean;

/**
 * @author RA324710
 *
 */

public interface WorkFlowReportExportService {

	StringBuilder populateCSVContent(List<WorkFlowReportBean> reportList);

}
