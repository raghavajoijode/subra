package org.subra.aem.foundation.account.services;

public interface SubraUserService {

	boolean isExistingUser(String email);
	
	String createUser(String email, String password, String name);
	
	String authenticateUser(String email, String password);

}
