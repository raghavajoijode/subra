package org.subra.aem.commons.servlets;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.utils.SubraDateTimeUtil;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=To test SubraJcrConstants",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + "/bin/subra/test/SubraDateTimeUtil" })
public class SubraJcrConstantsTestServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(SubraJcrConstantsTestServlet.class);

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.info("In Do Get Request");
		response.setContentType("text/html");
		LocalDateTime ldt = LocalDateTime.now();

		String s = "<h3>LDT: " + ldt + "</h3>";
		s = s + "<h3>LDT getZonedDateTime at IST: " + SubraDateTimeUtil.getZonedDateTime(ldt, ZoneId.of("IST"))
				+ "</h3>";
		s = s + "<h3>LDT getZonedDateTime at EST: " + SubraDateTimeUtil.getZonedDateTime(ldt, ZoneId.of("EST"))
				+ "</h3>";
		s = s + "<h3>LDT getZonedDateTime at UTC: " + SubraDateTimeUtil.getZonedDateTime(ldt, ZoneId.of("UTC"))
				+ "</h3>";
		s = s + "<h3>LDT localDateTimeAtUTC at IST: " + SubraDateTimeUtil.localDateTimeAtUTC(ldt) + "</h3>";

		response.getWriter().write(s);
	}

}
