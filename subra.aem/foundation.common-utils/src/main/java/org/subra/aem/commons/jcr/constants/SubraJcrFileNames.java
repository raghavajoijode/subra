package org.subra.aem.commons.jcr.constants;

public enum SubraJcrFileNames {

	FN_DEFAULT_TEXT_FILE("file.txt");

	private String value;

	private SubraJcrFileNames(final String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

}
