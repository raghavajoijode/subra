package org.subra.aem.commons.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Raghava Joijode
 *
 */
public class SubraStringUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubraStringUtils.class);
	public static final String COMMA = ",";
	public static final String HYPHEN = "-";
	public static final String SPACE = " ";
	public static final String SLASH = "/";
	public static final String COLON = ":";
	public static final String AT = "@";

	private SubraStringUtils() {
		throw new IllegalStateException(this.getClass().getSimpleName());
	}

	public static String writeValueAsString(Object value) {
		try {
			return new ObjectMapper().writeValueAsString(value);
		} catch (JsonProcessingException e) {
			LOGGER.error("Error Serializing Object...", e);
		}
		return null;
	}

	public static boolean isNoneBlank(String string, String... strings) {
		return StringUtils.isNotBlank(string) && Arrays.stream(strings).allMatch(StringUtils::isNotBlank);
	}

	public static boolean isNoneEmpty(String string, String... strings) {
		return StringUtils.isNotEmpty(string) && Arrays.stream(strings).allMatch(StringUtils::isNotEmpty);
	}

	public static boolean isAllBlank(String string, String... strings) {
		return !(StringUtils.isNotBlank(string) || Arrays.stream(strings).anyMatch(StringUtils::isNotBlank));
	}

	public static String[] getLookUpKeys(final String str) {
		List<String> lookUpkeys = SubraCollectionUtils.getStreamFromArray(StringUtils.substringsBetween(str, "${", "}"))
				.map(i -> i.contains(":-") ? StringUtils.substringBefore(i, ":-") : i).collect(Collectors.toList());
		return SubraCollectionUtils.toArray(lookUpkeys, ArrayUtils.EMPTY_STRING_ARRAY);
	}

	public static String encode(final String value, final String charset) throws UnsupportedEncodingException {
		return URLEncoder.encode(value, StringUtils.defaultIfBlank(charset, "UTF-8"));
	}

}