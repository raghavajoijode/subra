package org.subra.aem.mailer.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceRanking;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraHttpType;
import org.subra.aem.commons.constants.SubraUserMapperService;
import org.subra.aem.commons.exceptions.SubraCustomException;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.commons.jcr.constants.SubraJcrFileNames;
import org.subra.aem.commons.jcr.constants.SubraJcrPrimaryType;
import org.subra.aem.commons.jcr.constants.SubraJcrProperties;
import org.subra.aem.commons.jcr.utils.SubraResourceUtils;
import org.subra.aem.commons.utils.SubraCollectionUtils;
import org.subra.aem.commons.utils.SubraStringUtils;
import org.subra.aem.mailer.EmailRequest;
import org.subra.aem.mailer.Template;
import org.subra.aem.mailer.services.MailerService;
import org.subra.aem.mailer.services.TemplateService;

import com.day.cq.commons.jcr.JcrConstants;

/**
 * @author Raghava Joijode
 *
 */
@Component(service = TemplateService.class, immediate = true)
@ServiceRanking(60000)
@ServiceDescription("Subra - Template Service")
@Designate(ocd = TemplateServiceImpl.Config.class)
public final class TemplateServiceImpl implements TemplateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateServiceImpl.class);

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private MailerService mailerService;

	private ResourceResolver resolver;

	public static final String DEFAULT_APPROVED_TEMPLATES_PATH = "/conf/foundation/settings/mailer/templates/subra";
	public static final String DEFAULT_DRAFT_TEMPLATES_PATH = "/var/mailer/templates/draft";

	public static final String DEFAULT_DRAFT_TEMPLATES_ID_PREFIX = "DR_";
	public static final String DEFAULT_APPROVED_TEMPLATES_ID_PREFIX = "AP_";

	private Resource approvedTemplatesResource;
	private Resource draftTemplatesResource;
	private String draftTemplatesIDPrefix;
	private String approvedTemplatesIDPrefix;

	@ObjectClassDefinition(name = "Subra - Email Service", description = "Subra - Email Service")
	public @interface Config {

		@AttributeDefinition(name = "Approved Templates Path")
		String approved_templates_path()

		default DEFAULT_APPROVED_TEMPLATES_PATH;

		@AttributeDefinition(name = "Draft Templates Path")
		String draft_templates_path()

		default DEFAULT_DRAFT_TEMPLATES_PATH;

		@AttributeDefinition(name = "Draft Templates ID Prefix")
		String draft_templates_id_prefix()

		default DEFAULT_DRAFT_TEMPLATES_ID_PREFIX;

		@AttributeDefinition(name = "Approved Templates ID Prefix")
		String approved_templates_id_prefix() default DEFAULT_APPROVED_TEMPLATES_ID_PREFIX;
	}

	@Activate
	protected void activate(final Config config) {
		try {
			resolver = resolverFactory
					.getServiceResourceResolver(SubraResourceUtils.getAuthInfo(SubraUserMapperService.EMAIL_SERVICE));

			draftTemplatesIDPrefix = config.draft_templates_id_prefix();
			approvedTemplatesIDPrefix = config.approved_templates_id_prefix();

			draftTemplatesResource = SubraResourceUtils.getOrCreateResource(resolver, config.draft_templates_path(),
					SubraJcrPrimaryType.SLING_FOLDER);
			approvedTemplatesResource = SubraResourceUtils.getOrCreateResource(resolver,
					config.approved_templates_path(), SubraJcrPrimaryType.SLING_FOLDER);

		} catch (LoginException e) {
			LOGGER.error("Unable to get resource resolver...");
		}
	}

	@Override
	public String getDraftTemplatesIDPrefix() {
		return draftTemplatesIDPrefix;
	}

	@Override
	public String getApprovedTemplatesIDPrefix() {
		return approvedTemplatesIDPrefix;
	}

	@Override
	public List<Template> listTemplates() {
		List<Template> templates = new ArrayList<>();
		Iterator<Resource> approvedTemplatesItr = approvedTemplatesResource.listChildren();
		Iterator<Resource> draftTemplatesItr = draftTemplatesResource.listChildren();
		while (approvedTemplatesItr.hasNext()) {
			Template template = new Template(approvedTemplatesItr.next());
			template.setDraft(false);
			templates.add(template);
		}
		while (draftTemplatesItr.hasNext()) {
			templates.add(new Template(draftTemplatesItr.next()));
		}
		return templates;
	}

	@Override
	public Template getTemplate(final String id) throws SubraCustomException {
		StringBuilder pathBuilder = new StringBuilder();
		if (StringUtils.startsWith(id, approvedTemplatesIDPrefix))
			pathBuilder.append(approvedTemplatesResource.getPath()).append(SubraStringUtils.SLASH)
					.append(StringUtils.stripStart(id, approvedTemplatesIDPrefix));

		else if (StringUtils.startsWith(id, draftTemplatesIDPrefix))
			pathBuilder.append(draftTemplatesResource.getPath()).append(SubraStringUtils.SLASH)
					.append(StringUtils.stripStart(id, draftTemplatesIDPrefix));

		return Optional.of(resolver).map(r -> r.getResource(pathBuilder.toString())).map(Template::new)
				.orElseThrow(() -> new SubraCustomException("Invalid template ID..."));
	}

	@Override
	public String createOrUpdateTemplate(final String fileTitle, final String content) {
		String path = null;
		try {
			Session session = SubraResourceUtils.adoptToOrThrow(resolver, Session.class);
			InputStream contentIS = IOUtils.toInputStream(content, SubraHttpType.CHARSET_UTF_8.value());
			Binary binaryContent = session.getValueFactory().createBinary(contentIS);
			Node rootNode = SubraResourceUtils.adoptToOrThrow(draftTemplatesResource, Node.class);
			final String fileName = SubraCommonHelper.createNameFromTitle(fileTitle);
			if (!rootNode.hasNode(fileName)) {
				Node fileFolder = rootNode.addNode(fileName, JcrResourceConstants.NT_SLING_FOLDER);
				fileFolder.setProperty(SubraJcrProperties.PN_CREATED_BY.property(), session.getUserID());
				fileFolder.setProperty(SubraJcrProperties.PN_LOCKED.property(), false);

				Node fileNode = fileFolder.addNode(SubraJcrFileNames.FN_DEFAULT_TEXT_FILE.value(),
						JcrConstants.NT_FILE);
				Node fileContent = fileNode.addNode(JcrConstants.JCR_CONTENT, JcrConstants.NT_RESOURCE);
				fileContent.setProperty(JcrConstants.JCR_MIMETYPE, SubraHttpType.MEDIA_TYPE_TEXT.value());
				fileContent.setProperty(JcrConstants.JCR_DATA, binaryContent);
				path = fileNode.getPath();
			} else {
				Node fileNode = rootNode.getNode(fileName).getNode(SubraJcrFileNames.FN_DEFAULT_TEXT_FILE.value());
				fileNode.getNode(JcrConstants.JCR_CONTENT).setProperty(JcrConstants.JCR_DATA, binaryContent);
				path = fileNode.getPath();
			}
			session.save();
		} catch (IOException | RepositoryException e) {
			LOGGER.error("Error creating/updating template... ", e);
		} catch (Exception e) {
			LOGGER.error("Uncatched Error creating/updating template... ", e);
		}
		return path;
	}

	@Override
	public String readTemplate(final Template template) {
		return Optional.ofNullable(template).map(Template::getResource)
				.map(t -> t.getChild(SubraJcrFileNames.FN_DEFAULT_TEXT_FILE.value())).map(file -> {
					try {
						return IOUtils.toString(SubraResourceUtils.getFileAsIS(file),
								SubraHttpType.CHARSET_UTF_8.value());
					} catch (IOException | RepositoryException e) {
						LOGGER.error("Error getting content from template...", e);
					}
					return null;
				}).orElseThrow();
	}

	@Override
	public boolean deleteTemplate(final Template template) {
		boolean status = false;
		try {
			Resource templateResource = template.getResource();
			templateResource.getResourceResolver().delete(templateResource);
			status = true;
		} catch (PersistenceException e) {
			LOGGER.error("Error deleting template...", e);
		}
		return status;
	}

	@Override
	public String[] getLookUpKeys(final Template template) {
		return SubraStringUtils.getLookUpKeys(readTemplate(template));
	}

	@Override
	public EmailRequest generateRequestFormat(final Template template) {
		EmailRequest emailRequest = new EmailRequest();
		emailRequest.setTemplateId(
				(template.isDraft() ? draftTemplatesIDPrefix : approvedTemplatesIDPrefix) + template.getName());
		Map<String, String> params = new HashMap<>();
		params.put(MailerService.TO, "<to-emails-seperate-by-comma>");
		params.put(MailerService.CC, "<cc-emails-seperate-by-comma>");
		params.put(MailerService.BCC, "<bcc-emails-seperate-by-comma>");
		params.put(MailerService.SUBJECT, "<subject-line>");
		params.putAll(SubraCollectionUtils.getStreamFromArray(getLookUpKeys(template)).distinct()
				.collect(Collectors.toMap(k -> k, v -> "<value>")));
		emailRequest.setParams(params);
		return emailRequest;
	}

	@Override
	public String generateEmailMarkUp(final EmailRequest email) {
		try {
			StrSubstitutor substitutor = new StrSubstitutor(StrLookup.mapLookup(email.getParams()));
			return substitutor.replace(readTemplate(getTemplate(email.getTemplateId())));
		} catch (SubraCustomException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String> sendEmail(final EmailRequest email) {
		Template template;
		try {
			template = getTemplate(email.getTemplateId());
			return mailerService.sendEmail(template.getPath(), email.getParams());
		} catch (SubraCustomException e) {
			LOGGER.error("Error sending email...", e);
		}
		return Collections.emptyList();
	}

}
