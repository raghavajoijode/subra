package org.subra.aem.mailer.internal.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.helpers.SubraCommonHelper;
import org.subra.aem.commons.utils.SubraStringUtils;

import com.drew.lang.annotations.NotNull;

/**
 * @author Raghava Joijode
 *
 */
public class MailTemplateHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MailTemplateHelper.class);

	private String title;
	private String message;
	private String charset;

	private MailTemplateHelper() {
		throw new UnsupportedOperationException("Cannot create instance of " + this.getClass().getSimpleName());
	}

	private MailTemplateHelper(InputStream inputStream, String encoding, @NotNull String title) {
		this.charset = StringUtils.defaultIfBlank(encoding, "UTF-8");
		this.title = title;
		StringWriter sw = new StringWriter();
		try {
			IOUtils.copy(inputStream, sw, charset);
			this.message = sw.toString();
		} catch (IOException e) {
			LOGGER.error("Error reading message in the template...", e);
		}
	}

	public static MailTemplateHelper create(final String templatePath, Session session) {
		if (StringUtils.isNotBlank(templatePath)) {
			final String contentNodePath = templatePath + "/jcr:content";

			try {
				if (session.itemExists(contentNodePath)) {
					Node contentNode = session.getNode(contentNodePath);
					if (contentNode.hasProperty("jcr:data")) {
						Property nodeData = contentNode.getProperty("jcr:data");
						return new MailTemplateHelper(nodeData.getBinary().getStream(),
								contentNode.hasProperty("jcr:encodiing")
										? contentNode.getProperty("jcr:encoding").getString()
										: "UTF-8",
								SubraCommonHelper.decodeTitleFromName(contentNode.getName()));
					} else {
						throw new PathNotFoundException("Property Missing..");
					}
				} else {
					throw new IllegalArgumentException("Template doesnt exist");
				}
			} catch (RepositoryException e) {
				LOGGER.error("Error creating MailTemplate Object", e);
			}
		} else {
			throw new IllegalArgumentException("Template path is not provided");
		}

		return null;
	}

	public <T extends Email> Email getEmail(StrLookup lookup, Class<T> type) throws EmailException {
		StrSubstitutor substitutor = new StrSubstitutor(lookup);
		final String msg = substitutor.replace(message);
		if (type.equals(HtmlEmail.class)) {
			HtmlEmail email = new HtmlEmail();
			email.setHtmlMsg(msg);
			return email;
		} else if (type.equals(SimpleEmail.class)) {
			SimpleEmail email = new SimpleEmail();
			email.setMsg(msg);
			return email;
		}
		return new SimpleEmail();
	}

	public String getMessage() {
		return message;
	}

	public String getTitle() {
		return title;
	}

	public String[] getLookUpKeys() {
		return SubraStringUtils.getLookUpKeys(message);
	}

}
