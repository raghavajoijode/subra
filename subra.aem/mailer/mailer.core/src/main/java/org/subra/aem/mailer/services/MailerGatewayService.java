package org.subra.aem.mailer.services;

import org.apache.commons.mail.Email;

/**
 * @author Raghava Joijode
 *
 */
public interface MailerGatewayService {

	boolean send(Email email);

}
