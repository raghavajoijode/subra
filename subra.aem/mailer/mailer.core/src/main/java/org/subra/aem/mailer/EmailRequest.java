package org.subra.aem.mailer;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.helpers.SubraCommonHelper;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Raghava Joijode
 *
 */
public class EmailRequest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailRequest.class);

	private String templateId;

	private Map<String, String> params;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		try {
			return SubraCommonHelper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}

}