package org.subra.aem.mailer.services;

import java.util.List;
import java.util.Map;

import javax.activation.DataSource;

/**
 * @author Raghava Joijode
 *
 */
public interface MailerService {

	/**
	 * Sender Email Address variable passed in the input parameter map to the
	 * sendEmail() function.
	 */
	public static final String SENDER_EMAIL_ADDRESS = "senderEmailAddress";

	public static final String TO = "to";

	public static final String CC = "cc";

	public static final String BCC = "bcc";

	/**
	 * Sender Name variable passed in the input parameter map to the sendEmail()
	 * function.
	 */
	public static final String SENDER_NAME = "senderName";

	/**
	 * Subject line variable used to specify the subject in the input parameter map.
	 */
	public static final String SUBJECT = "subject";

	/**
	 * Variable used to specify the bounce address. Also referred to as the envelope
	 * FROM address.
	 */
	public static final String BOUNCE_ADDRESS = "bounceAddress";

	List<String> sendEmail(MailerGatewayService messageGateway, String templatePath, Map<String, String> emailParams,
			String... recipients);

	List<String> sendEmail(MailerGatewayService messageGateway, String templatePath, Map<String, String> emailParams,
			Map<String, DataSource> attachments, String... recipients);

	List<String> sendEmail(String templatePath, Map<String, String> emailParams, String... recipients);

	List<String> sendEmail(String templatePath, Map<String, String> emailParams);

	List<String> sendEmail(String templatePath, Map<String, String> emailParams, Map<String, DataSource> attachments,
			String... recipients);

}
