package org.subra.aem.mailer.models;

/**
 * @author Raghava Joijode
 *
 */
public interface TemplateModel {

	default String getMessage() {
		throw new UnsupportedOperationException();
	}

	default String[] getLookUpKeys() {
		throw new UnsupportedOperationException();
	}

}
