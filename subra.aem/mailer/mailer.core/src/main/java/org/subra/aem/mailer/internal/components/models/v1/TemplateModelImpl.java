package org.subra.aem.mailer.internal.components.models.v1;

import javax.annotation.PostConstruct;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.subra.aem.mailer.internal.helpers.MailTemplateHelper;
import org.subra.aem.mailer.models.TemplateModel;


/**
 * @author Raghava Joijode
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class }, adapters = { TemplateModel.class })
public class TemplateModelImpl implements TemplateModel {

	@Self
	private SlingHttpServletRequest request;

	@RequestAttribute
	private String templatePath;

	private MailTemplateHelper mailTemplate;

	@PostConstruct
	protected void init() {
		mailTemplate = MailTemplateHelper.create(templatePath, request.adaptTo(Session.class));
	}

	@Override
	public String getMessage() {
		System.out.println(mailTemplate.getMessage());
		return mailTemplate.getMessage();
	}

	@Override
	public String[] getLookUpKeys() {
		return mailTemplate.getLookUpKeys();
	}

}
