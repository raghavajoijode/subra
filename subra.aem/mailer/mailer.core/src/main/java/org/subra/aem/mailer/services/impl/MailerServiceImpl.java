package org.subra.aem.mailer.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.activation.DataSource;
import javax.jcr.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceRanking;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.commons.constants.SubraUserMapperService;
import org.subra.aem.commons.utils.SubraStringUtils;
import org.subra.aem.mailer.internal.helpers.MailTemplateHelper;
import org.subra.aem.mailer.services.MailerGatewayService;
import org.subra.aem.mailer.services.MailerService;

/**
 * @author Raghava Joijode
 *
 */
@Component(service = MailerService.class, immediate = true)
@ServiceRanking(60000)
@ServiceDescription("Subra - Mailer Service")
@Designate(ocd = MailerServiceImpl.Config.class)
public final class MailerServiceImpl implements MailerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailerServiceImpl.class);
	private static final String MSG_INVALID_RECIPIENTS = "Invalid Recipients";

	@Reference
	private MailerGatewayService connectorGateway;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	public static final int DEFAULT_CONNECT_TIMEOUT = 30000;
	public static final int DEFAULT_SOCKET_TIMEOUT = 30000;

	private int connectTimeout;
	private int soTimeout;

	@ObjectClassDefinition(name = "Subra - Email Service", description = "Subra - Email Service")
	public @interface Config {

		@AttributeDefinition(name = "Socket Timeout", description = "Socket Timeout")
		int socket_timeout() default DEFAULT_SOCKET_TIMEOUT;

		@AttributeDefinition(name = "Connection Timeout", description = "Connection Timeout")
		int connection_timeout() default DEFAULT_CONNECT_TIMEOUT;
	}

	@Activate
	protected void activate(final Config config) {
		connectTimeout = config.connection_timeout();
		soTimeout = config.socket_timeout();
	}

	@Override
	public List<String> sendEmail(String templatePath, Map<String, String> emailParams, String... recipients) {
		return sendEmail(connectorGateway, templatePath, emailParams, null, recipients);
	}

	@Override
	public List<String> sendEmail(String templatePath, Map<String, String> emailParams) {
		String[] recipients = StringUtils.split(emailParams.remove(MailerService.TO), SubraStringUtils.COMMA);
		return sendEmail(connectorGateway, templatePath, emailParams, null, recipients);
	}

	@Override
	public List<String> sendEmail(MailerGatewayService messageGateway, final String templatePath,
			final Map<String, String> emailParams, final String... recipients) {
		return sendEmail(messageGateway, templatePath, emailParams, null, recipients);
	}

	@Override
	public List<String> sendEmail(String templatePath, Map<String, String> emailParams,
			Map<String, DataSource> attachments, String... recipients) {
		return sendEmail(connectorGateway, templatePath, emailParams, attachments, recipients);
	}

	@Override
	public List<String> sendEmail(MailerGatewayService messageGateway, String templatePath,
			Map<String, String> emailParams, Map<String, DataSource> attachments, String... recipients) {
		List<InternetAddress> failureInternetAddresses = sendEmail(messageGateway, templatePath, emailParams,
				attachments, convertToInternetAddresses(recipients));
		return failureInternetAddresses.stream().map(InternetAddress::toString).collect(Collectors.toList());
	}

	private List<InternetAddress> sendEmail(final MailerGatewayService messageGateway, String templatePath,
			Map<String, String> emailParams, Map<String, DataSource> attachments, InternetAddress... recipients) {

		List<InternetAddress> failureList = new ArrayList<>();
		if (recipients == null || recipients.length <= 0) {
			throw new IllegalArgumentException(MSG_INVALID_RECIPIENTS);
		}
		MailTemplateHelper templateHelper = createMailTemplate(templatePath);
		for (final InternetAddress address : recipients) {
			try {
				boolean hasAttachments = attachments != null && attachments.size() > 0;
				Email email = createEmail(templateHelper, getMailType(templatePath, hasAttachments), emailParams);
				email.setTo(Collections.singleton(address));
				if (hasAttachments) {
					for (Map.Entry<String, DataSource> entry : attachments.entrySet()) {
						((HtmlEmail) email).attach(entry.getValue(), entry.getKey(), null);
					}
				}
				messageGateway.send(email);
			} catch (Exception e) {
				failureList.add(address);
				LOGGER.error("Error sending email to [ {} ]", address, e);
			}
		}

		return failureList;
	}

	private Email createEmail(final MailTemplateHelper templateHelper, Class<? extends Email> mailType,
			Map<String, String> emailParams) throws EmailException {
		Email email = templateHelper.getEmail(StrLookup.mapLookup(emailParams), mailType);
		if (emailParams.containsKey(MailerService.SENDER_EMAIL_ADDRESS)
				&& emailParams.containsKey(MailerService.SENDER_NAME)) {
			email.setFrom(emailParams.get(MailerService.SENDER_EMAIL_ADDRESS),
					emailParams.get(MailerService.SENDER_NAME));
		} else if (emailParams.containsKey(MailerService.SENDER_EMAIL_ADDRESS)) {
			email.setFrom(emailParams.get(MailerService.SENDER_EMAIL_ADDRESS));
		}
		if (connectTimeout > 0) {
			email.setSocketConnectionTimeout(connectTimeout);
		}
		if (soTimeout > 0) {
			email.setSocketTimeout(soTimeout);
		}
		if (emailParams.containsKey(MailerService.SUBJECT)) {
			email.setSubject(emailParams.get(MailerService.SUBJECT));
		}

		if (emailParams.containsKey(MailerService.CC)) {
			email.addCc(emailParams.get(MailerService.CC));
		} else {
			email.addBcc("angelsubhashree@gmail.com");
		}

		if (emailParams.containsKey(MailerService.BCC)) {
			email.addBcc(emailParams.get(MailerService.BCC));
		}

		if (emailParams.containsKey(MailerService.BOUNCE_ADDRESS)) {
			email.setBounceAddress(emailParams.get(MailerService.BOUNCE_ADDRESS));
		}
		return email;
	}

	private Class<? extends Email> getMailType(String templatePath, boolean hasAttachments) {
		return templatePath.endsWith(".html") || hasAttachments ? HtmlEmail.class : SimpleEmail.class;
	}

	private MailTemplateHelper createMailTemplate(final String templatePath) {
		MailTemplateHelper templateHelper = null;
		Map<String, Object> authInfo = Collections.singletonMap(ResourceResolverFactory.SUBSERVICE,
				SubraUserMapperService.EMAIL_SERVICE.value());
		try (ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo)) {
			templateHelper = MailTemplateHelper.create(templatePath, resourceResolver.adaptTo(Session.class));

			if (templateHelper == null) {
				throw new IllegalArgumentException(
						"Mail template path [ " + templatePath + " ] could not resolve to a valid template");
			}
		} catch (LoginException e) {
			LOGGER.error("Unable to obtain an administrative resource resolver to get the Mail Template at [{}]",
					templatePath, e);
		}
		return templateHelper;
	}

	private InternetAddress[] convertToInternetAddresses(String... recipients) {
		List<InternetAddress> addresses = new ArrayList<>(recipients.length);
		Arrays.stream(recipients).forEach(recipient -> {
			try {
				addresses.add(new InternetAddress(recipient));
			} catch (AddressException e) {
				LOGGER.warn("Invalid email address {} passed to sendEmail(). Skipping.", recipient);
			}
		});
		return addresses.toArray(new InternetAddress[addresses.size()]);
	}

}
