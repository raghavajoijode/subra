package org.subra.aem.mailer;

import org.apache.sling.api.resource.Resource;
import org.subra.aem.commons.jcr.SubraResource;

/**
 * @author Raghava Joijode
 *
 */
public class Template extends SubraResource {

	private boolean isDraft = true;

	public Template(Resource resource) {
		super(resource);
	}

	public boolean isDraft() {
		return isDraft;
	}

	public void setDraft(boolean isDraft) {
		this.isDraft = isDraft;
	}

}