import React, { useReducer } from 'react'
import axios from 'axios'
import AppContext, { AppReducers as Reducer } from './appContext'
import {
    SET_DEFAULT_USERS,
    SET_USERS_FROM_DEFAULT_USERS,
    SET_SEARCH_USERS,
    SET_USER,
    SET_USER_REPOS,
    CLEAR_SEARCH_USERS,
    SET_LOADING
} from '../types'

const AppState = props => {

    let githubClientId = process.env.REACT_APP_GIT_API_CLIENT_ID
    let githubClientSecret = process.env.REACT_APP_GIT_API_CLIENT_SECRET
    let githubUsersApi = process.env.REACT_APP_GIT_USERS_API
    let githubUsersSearchApi = process.env.REACT_APP_GIT_USERS_SEARCH_API

    /* if (process.env.NODE_ENV === 'production') {
        githubClientId = process.env.GIT_API_CLIENT_ID
        githubClientSecret = process.env.GIT_API_CLIENT_SECRET
    } else {
        githubClientId = process.env.REACT_APP_GIT_API_CLIENT_ID
        githubClientSecret = process.env.REACT_APP_GIT_API_CLIENT_SECRET
    } */


    // Initial State values
    const initialState = {
        defaultUsers: [],
        searchUsers: [],
        users: [],
        user: {},
        repos: [],
        loading: false
    }

    //Creating reducer connection
    const [state, dispatch] = useReducer(Reducer, initialState)

    //Heler methods
    const clearUsers = () => dispatch({ type: CLEAR_SEARCH_USERS })
    const setLoading = () => dispatch({ type: SET_LOADING })
    const getDefaultUsers = async () => {
        setLoading()
        const result = await axios.get(`${githubUsersApi}?client_id=${githubClientId}&client_secret=${githubClientSecret}`)
        dispatch({
            type: SET_DEFAULT_USERS,
            payload: result.data
        })
    }
    const loadDefaultUsers = () => {
        if (state.defaultUsers.length === 0)
            getDefaultUsers()
        else {
            dispatch({
                type: SET_USERS_FROM_DEFAULT_USERS,
                payload: state.defaultUsers
            })
        }

    }
    const getSearchedUsers = async text => {
        setLoading()
        const result = await axios.get(`${githubUsersSearchApi}?q=${text}&client_id=${githubClientId}&client_secret=${githubClientSecret}`)
        dispatch({
            type: SET_SEARCH_USERS,
            payload: result.data.items
        })
    }
    const getUser = async username => {
        setLoading()
        const result = await axios.get(`${githubUsersApi}/${username}?client_id=${githubClientId}&client_secret=${githubClientSecret}`)
        dispatch({
            type: SET_USER,
            payload: result.data
        })
    }
    const getUserRepos = async username => {
        setLoading()
        const result = await axios.get(`${githubUsersApi}/${username}/repos?per_page=5&sort=created:asc&client_id=${githubClientId}&client_secret=${githubClientSecret}`)
        dispatch({
            type: SET_USER_REPOS,
            payload: result.data
        })
    }

    return (
        <AppContext.Provider value={
            {
                defaultUsers: state.defaultUsers,
                searchUsers: state.searchUsers,
                users: state.users,
                user: state.user,
                repos: state.repos,
                loading: state.loading,
                clearUsers,
                loadDefaultUsers,
                getSearchedUsers,
                getUser,
                getUserRepos
            }
        }>
            {props.children}
        </AppContext.Provider>
    )
}

export default AppState