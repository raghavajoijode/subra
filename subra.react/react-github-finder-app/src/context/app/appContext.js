import { createContext } from 'react'
import {
    SET_DEFAULT_USERS,
    SET_USERS_FROM_DEFAULT_USERS,
    SET_SEARCH_USERS,
    SET_USER,
    SET_USER_REPOS,
    CLEAR_SEARCH_USERS,
    SET_LOADING
} from '../types'

export const AppReducers = (state, action) => {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        case SET_USERS_FROM_DEFAULT_USERS:
            return {
                ...state,
                users: action.payload
            }
        case SET_DEFAULT_USERS:
            return {
                ...state,
                loading: false,
                defaultUsers: action.payload,
                users: action.payload,
                searchUsers: []
            }
        case SET_SEARCH_USERS:
            return {
                ...state,
                loading: false,
                searchUsers: action.payload,
                users: action.payload
            }
        case CLEAR_SEARCH_USERS:
            return {
                ...state,
                loading: false,
                users: [],
                searchUsers: []
            }
        case SET_USER:
            return {
                ...state,
                loading: false,
                user: action.payload
            }
        case SET_USER_REPOS:
            return {
                ...state,
                loading: false,
                repos: action.payload
            }
        default:
            return state
    }

}

export default createContext()