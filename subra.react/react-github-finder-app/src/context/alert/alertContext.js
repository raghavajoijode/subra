import { createContext } from "react";
import { TRIGGER_ALERT } from '../types'

export const alertReducer = (state, action) => {
    switch (action.type) {
        case TRIGGER_ALERT:
            return {
                ...state,
                alert: action.payload
            }
        default:
            return state
    }
}

export default createContext()