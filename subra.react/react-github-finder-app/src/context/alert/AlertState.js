import React, { useReducer } from 'react'
import AlertContext, { alertReducer } from './alertContext'
import { TRIGGER_ALERT } from '../types'

const AlertState = props => {

    const initialState = {
        alert: null
    }

    const [state, dispatch] = useReducer(alertReducer, initialState)

    const triggerAlert = (msg, type) => {
        setAlert({ msg, type })
        setTimeout(() => setAlert(null), 5000)
    }
    const setAlert = (value) => dispatch({
        type: TRIGGER_ALERT,
        payload: value
    })

    return (
        <AlertContext.Provider
            value={
                {
                    alert: state.alert,
                    triggerAlert
                }
            }
        >
            {props.children}
        </AlertContext.Provider>
    )
}

export default AlertState