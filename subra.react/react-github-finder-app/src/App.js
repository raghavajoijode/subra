import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Header from './components/layout/Header'
import Home from './components/pages/Home'
import Search from './components/pages/Search'
import NotFound from './components/pages/NotFound'
import User from './components/users/User'
import AppState from './context/app/AppState'
import './styles/App.css'

const App = () => (
  <AppState>
    <Router>
      <div className='App'>
        <Header title='Subra GT Finder' />
        <div className='container'>
          <Switch>
            <Route exact path='/search' component={Search} />
            <Route exact path='/' component={Home} />
            <Route exact path='/user/:uname' component={User} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </Router>
  </AppState>
)

export default App