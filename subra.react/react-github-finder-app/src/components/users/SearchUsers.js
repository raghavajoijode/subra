import React, { Fragment, useState, useEffect, useContext } from 'react'
import AppContext from '../../context/app/appContext'
import AlertContext from '../../context/alert/alertContext'

const SearchUsers = () => {

    const { clearUsers, getSearchedUsers, searchUsers } = useContext(AppContext)
    const { triggerAlert } = useContext(AlertContext)

    useEffect(() => {
        clearUsers();
        //eslint-disable-next-line
    }, []);

    const [text, setText] = useState('')

    const initiateUsersSearch = e => {
        e.preventDefault();
        if (text === '')
            triggerAlert('Please enter text', 'light')
        else {
            getSearchedUsers(text)
            setText('')
        }
    }
    const changeValue = e => setText(e.target.value)
    const consoleFn = () => console.log('Search Loaded')
    return (
        <Fragment>
            <form className='form' onLoad={consoleFn}>
                <input type='text' name='text' placeholder='Search Users' value={text} onChange={changeValue} />
                <input type='submit' className='btn btn-dark btn-block' value='Search' onClick={initiateUsersSearch} />
            </form>
            {searchUsers.length > 1 && (
                <button className='btn btn-light btn-block' onClick={clearUsers}>Clear</button>
            )}
        </Fragment>
    )
}

export default SearchUsers