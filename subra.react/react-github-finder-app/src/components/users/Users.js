import React, { useContext } from 'react'
import UserItem from './UserItem'
import Spinner from '../layout/Spinner'
import AppContext from '../../context/app/appContext'

export default () => {

    const { loading, users } = useContext(AppContext)

    if (loading) return <Spinner />

    return (
        <div style={userStyle}>
            {users.map(user => (
                <UserItem key={user.login} user={user} />
            ))}
        </div>
    )
}

const userStyle = {
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 1fr)',
    gridGap: '1rem'
}
