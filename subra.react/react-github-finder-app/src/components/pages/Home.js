import React, { useEffect, useContext } from 'react'
import AppContext from '../../context/app/appContext'
import Users from '../users/Users'

const Home = () => {
    const { loadDefaultUsers } = useContext(AppContext)
    useEffect(
        () => {
            loadDefaultUsers()
            //eslint-disable-next-line
        }, []
    )
    return (
        <Users />
    )
}

export default Home