import React from 'react'
import AlertState from '../../context/alert/AlertState'
import SearchUsers from '../users/SearchUsers'
import Users from '../users/Users'
import Alert from '../layout/Alert'

export default () => {
    return (
        <AlertState>
            <Alert />
            <SearchUsers />
            <Users />
        </AlertState>
    )
}
