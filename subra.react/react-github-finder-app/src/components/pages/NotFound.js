import React from 'react'

export default () => (
    <div className='lead'>
        <h1>Not Found</h1>
        <p>The page you have request is not found, please verify the url requested.
                <br></br>
                    Or you can use the menu options to navigate to any of page available.</p>
    </div>
)
