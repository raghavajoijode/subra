import React from "react"
import PropTypes from 'prop-types'
import { Link } from "react-router-dom"

const Header = ({ title, icon }) => (
    <nav className='navbar bg-primary'>
        <h2><i className={icon} />{title}</h2>
        <ul>
            <li>
                <Link to='/'>Home</Link>
            </li>
            <li>
                <Link to='/search'>Search</Link>
            </li>
        </ul>
    </nav>
);

Header.defaultProps = {
    title: 'Github Finder',
    icon: 'fab fa-github'
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired
}

export default Header