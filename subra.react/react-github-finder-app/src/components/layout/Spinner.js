import React from 'react'
import spinner from './spinner.gif'

export default () =>
    <img
        alt='Loading...'
        src={spinner}
        style={sprinnerStyle}
    />

const sprinnerStyle = {
    display: 'block',
    width: '200px',
    margin: 'auto'
}
