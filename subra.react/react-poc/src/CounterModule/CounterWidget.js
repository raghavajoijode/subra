import React from 'react';

class CounterWidget extends React.Component {

    constructor(){
        super();
        this.state = {
            counter : 0
        }
    }

    render(){
        let state = this.state;
        let counter = state.counter;
        return (
            <div>
                <p onClick = {this.incrementCounter.bind(this)} >Increase</p> 
                <p onClick = {this.decrementCounter.bind(this)} >Decrease</p>
                <p>Counter Value is : {counter}</p>
            </div>
        )
    }

    incrementCounter () {
        this.setState({
                counter : this.state.counter + 1
        })
    }
    
    decrementCounter () {
        this.setState({
                counter : this.state.counter - 1
        })
    }
}

export default CounterWidget;