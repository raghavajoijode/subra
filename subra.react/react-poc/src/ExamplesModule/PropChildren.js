import React from 'react'; 

const PropChildren = (props) => { return (
        <div>
            <h2>This is From ExamplesModule/PropChildren</h2>
            <h4>{props.children}</h4>
            <p>{props.text}</p>
        </div> 
    )
}

//Custom Prop Validation
PropChildren.propTypes = {    
    text (props, propName, Component) {
        if(!(propName in props)){
            return new Error( `${propName} is Missinggggg.`);
        }
    }

}

export default PropChildren;
