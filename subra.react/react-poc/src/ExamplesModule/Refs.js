import React from 'react';

class Refs extends React.Component {

    constructor(){

        super();
        this.state = {

            firstInput : '',
            secondInput : '',
            thirdInput : ''
        }
    }

    update () {
        this.setState({
            firstInput: this.refs.first.value,
            secondInput: this.second.value,
            thirdInput: this.c.refs.compInp.value
        })
    }
    render(){
        return (
            <div>
                <div>
                    <h2>This is Refs Example</h2>
                </div>
                <div>
                    <input ref = "first" type = "text" 
                        onChange = {this.update.bind(this)}
                    />
                    {this.state.firstInput}
                    <hr/>
                    <input 
                        ref = { node => this.second = node}
                        type = "text" 
                        onChange = {this.update.bind(this)}
                    />
                    {this.state.secondInput}
                    <Input
                        ref = {comp => this.c = comp}
                        update = {this.update.bind(this)}
                    />
                    {this.state.thirdInput}
                </div>
            </div>
        )

    }

    
    

}

class Input extends React.Component{
    render (){
        return (
            <div>
                <input
                    type="text"
                    ref = "compInp"
                    onChange={this.props.update}
                />
            </div>
        )
    }
}

export default Refs;