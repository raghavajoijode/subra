import React from 'react';
import PropTypes from 'prop-types';
import StateLess from './ExamplesModule/StateLess';
import PropChildren from './ExamplesModule/PropChildren';
import Refs from './ExamplesModule/Refs';
// Renamed To 'IncrementWidget' instead of 'CounterWidget'
import IncrementWidget from './CounterModule/CounterWidget'; 

class App extends React.Component {
    
    constructor(){
        super();
        this.state = {
            inpVal: "No Value entered"
        }
    }
      
    render(){
        return (
            <div>
                <h1>{this.props.txt}</h1>
                <p> {this.props.compName}</p>
                <input type="text" onChange={this.update.bind(this)}/>
                <p>Entered Value:- {this.state.inpVal}</p>

                <StateLess/>

                <IncrementWidget/>

                <PropChildren text= "Hello I am app-2">I am from App2 Component - Example for Props.child</PropChildren>
                
                <Refs/>
            </div>
        )
    }
    
    update (e) {
        this.setState({
                inpVal : e.target.value
        })
    }
}


 
// Props Validation
App.propTypes = {
   // txt: PropTypes.string.isRequired, //-> OOTB validation
   // This is Custom Validation
    txt(props, propName, Component){
        if(!(propName in props)) {
            return new Error(`${propName} not available`)
        }
    },
    num: PropTypes.number//-> OOTB validation
}

// TO Set Default Values to Props
App.defaultProps = {
    compName: "I am App component",
    num: 5
}

export default App;