Step 1:
Download "golang" - https://golang.org/dl/
Version at time of this note is : go1.15.2

Install it to a location: my location - C:\Users\ragha\development\tools\Go
Make sure C:\Users\ragha\development\tools\G0\bin is added to path variable

To verify installation, open terminal and hit "go version" - This should provide version details

Documentation: https://golang.org/doc/
Packages: https://golang.org/pkg/

