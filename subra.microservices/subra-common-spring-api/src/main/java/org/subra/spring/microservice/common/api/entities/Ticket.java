package org.subra.spring.microservice.common.api.entities;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.subra.common.api.dtos.TicketDto;

import lombok.ToString;

@SuppressWarnings("serial")
@ToString

@Document(collection = "tickets")
public class Ticket extends TicketDto implements Serializable {

}
