package org.subra.spring.microservice.common.api.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.subra.common.api.dtos.UserDto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Getter
@Setter
@ToString

@Document(collection = "users")
public class User extends UserDto implements Serializable, UserDetails {

	@JsonIgnore private String password;

	public User(final String email, final String password, final String name) {
		this.setEmail(email);
		this.setPassword(password);
		this.setName(name);
		this.setCreatedDate(LocalDateTime.now());
	}

	public static User create(final String email, final String password, final String name) {
		return new User(email, password, StringUtils.defaultIfBlank(name, email));
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.emptyList();
	}

	@Override
	public String getUsername() {
		return getEmail();
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}

}
