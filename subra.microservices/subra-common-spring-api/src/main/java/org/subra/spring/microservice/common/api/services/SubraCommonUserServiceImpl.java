package org.subra.spring.microservice.common.api.services;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.subra.spring.microservice.common.api.entities.UserEntity;
import org.subra.spring.microservice.common.api.repos.SubraUsersRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SubraCommonUserServiceImpl implements UserDetailsService {

	@Autowired private SubraUsersRepository repository;

	@PostConstruct
	protected void active() {
		log.debug("SubraCommonUserServiceImpl started succesfully");
	}

	// Loads user from repository using email
	public UserEntity loadUser(String email) {
		return repository.findByEmail(email).orElse(null);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = loadUser(username);
		if (userEntity == null || StringUtils.isBlank(userEntity.getEmail())) {
			throw new UsernameNotFoundException("Cannot find email.");
		}
		return userEntity;
	}
	
}
