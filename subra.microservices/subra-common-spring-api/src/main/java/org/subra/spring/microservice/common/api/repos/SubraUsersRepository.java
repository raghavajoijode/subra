package org.subra.spring.microservice.common.api.repos;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.subra.spring.microservice.common.api.entities.UserEntity;

@Repository
public interface SubraUsersRepository extends MongoRepository<UserEntity, String> {
	Optional<UserEntity> findByEmail(String email);
	Optional<UserEntity> findByAccountId(String accountId);
	void deleteByAccountId(String accountId);
}
