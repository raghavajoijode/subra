package org.subra.spring.microservice.common.api.services;

import static java.util.Collections.emptyList;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.subra.common.api.utils.SubraJwtTokenUtil;
import org.subra.spring.microservice.common.api.entities.UserEntity;

import lombok.extern.slf4j.Slf4j;

@Service
@Configuration
@Slf4j
@Import(SubraCommonUserServiceImpl.class)
public class SubraJwtTokenService {
	
	private static final String TOKEN_PREFIX = "Bearer";
	private static final String AUTHORIZATION_HEADER = "Authorization";
	
	@Value("${subra.jwt.expiryInMs}")
	private long jwtExpirationInMs;

	@Value(value = "${subra.jwt.secret}")
	private String jwtSecret;

	@Autowired
	private SubraCommonUserServiceImpl userService;

	@PostConstruct
	protected void activate() {
		log.info("JwtUtils activated with secret {}", jwtSecret);
		SubraJwtTokenUtil.configure(jwtExpirationInMs, jwtSecret);
	}

	/*
	 * Returns JWT Token from Authentication
	 */
	public String generateTokenWithAuthentication(Authentication authentication) {
		UserEntity userEntity = (UserEntity) authentication.getPrincipal();
		return SubraJwtTokenUtil.generateTokenWithUserName(userEntity.getEmail(), userEntity.getAccountId());
	}

	/*
	 * Returns Authentication from request i.e Token
	 */
	public Authentication getAuthentication(HttpServletRequest request) {
		String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
		if (authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX)) {
			final String token = extractTokenFromAuthorizationHeader(authorizationHeader);
			return getAuthentication(token);
		}
		return null;
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = null;
		try {
			userDetails = userService.loadUserByUsername(extractUserName(token));
		} catch (UsernameNotFoundException e) {
			log.error("Exception Occured", e);
		}
		return userDetails != null && validateToken(userDetails, token)
				? new UsernamePasswordAuthenticationToken(userDetails, null, emptyList())
				: null;
	}

	/*
	 * Returns JWT Token from Authentication
	 */
	public Boolean validateToken(UserDetails userDetails, String token) {
		return StringUtils.equals(extractUserName(token), userDetails.getUsername()) && !SubraJwtTokenUtil.isTokenExpired(token);
	}

	/*
	 * Return UserName i.e. Subject from Authentication Header value by trimming it
	 * to JWT Token
	 */
	public String extractUserName(String token) {
		return SubraJwtTokenUtil.extractUserName(token);
	}
	
	public String extractAccountId(String token) {
		return SubraJwtTokenUtil.extractAccid(token);
	}

	/*
	 * gets exact JWT token from Authentication header
	 */
	private String extractTokenFromAuthorizationHeader(String token) {
		return token.replace(TOKEN_PREFIX, "").trim();
	}
}
