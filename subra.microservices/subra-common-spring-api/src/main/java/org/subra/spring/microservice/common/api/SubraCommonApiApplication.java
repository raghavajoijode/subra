package org.subra.spring.microservice.common.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubraCommonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraCommonApiApplication.class, args);
	}
	
}
