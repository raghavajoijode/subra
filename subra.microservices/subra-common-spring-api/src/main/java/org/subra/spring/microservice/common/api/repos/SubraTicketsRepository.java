package org.subra.spring.microservice.common.api.repos;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.subra.spring.microservice.common.api.entities.Ticket;

@Repository
public interface SubraTicketsRepository extends MongoRepository<Ticket, Long> {

	Optional<Ticket> findByTicketNumber(String number);

}
