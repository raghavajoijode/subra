package org.subra.spring.microservice.gateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayRoutes {
	@Bean
	public RouteLocator ticketsRouteLocator(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r -> r.path("/subra/api/v1/users/**").filters(f -> f.stripPrefix(1)).uri("http://localhost:8301"))
				.route(r -> r.path("/subra/api/v1/tickets/**").filters(f -> f.stripPrefix(1)).uri("http://localhost:8303"))
				.build();
	}
}