package org.subra.spring.microservice.mail.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.subra.common.api.dtos.EmailPayLoad;

@RestController
@RequestMapping("/api/v1/email")
public class Controller {
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@GetMapping("/testservice")
	public String testAPI() {
		return "I am active";
	}
	
	@GetMapping("/simple/{email}")
	public String sendEmailAPI(@PathVariable("email") String email) {
		EmailPayLoad p = new EmailPayLoad();
		p.setToAddress(email);
		p.setSubject("Frome mial service");
		p.setBody("hello");
		rabbitTemplate.convertAndSend("SimpleEmail", p);
		return "Success";
	}
}
