package org.subra.spring.microservice.mail.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.subra.common.api.dtos.EmailPayLoad;

@Service
public class EmailServiceImpl {

	@Autowired
	public JavaMailSender mailSender;
	
	
	@RabbitListener(queues = "SimpleEmail")
	public void getMessage(EmailPayLoad p) {
		System.out.println("we are sending email 2"+ p.getToAddress());
		sendSimpleMessage(p.getToAddress(), p.getSubject(), p.getBody());
	}
	
	
	private void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setSubject(subject);
		message.setText(text);
		mailSender.send(message);
	}
}