package org.subra.spring.microservice.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubraMailServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraMailServiceApplication.class, args);
	}

}
