package org.subra.spring.microservice.ticketing.documents;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Getter
@Setter
@ToString

@Document(collection = "tickets")
public class Ticket implements Serializable {

	private long id;
	private String ticketNumber;
	private String heading;
	private String description;
	private String status;
	private List<Update> updates;
	private int reporterId;
	private int assigneeId;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;
}
