package org.subra.spring.microservice.ticketing.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.subra.spring.microservice.ticketing.documents.User;

@Repository
public interface UsersRepository extends MongoRepository<User, String> {
	Optional<User> findByEmail(String userName);
}
