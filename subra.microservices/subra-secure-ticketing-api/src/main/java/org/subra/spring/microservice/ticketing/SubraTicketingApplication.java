package org.subra.spring.microservice.ticketing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubraTicketingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraTicketingApplication.class, args);
	}
	
}
