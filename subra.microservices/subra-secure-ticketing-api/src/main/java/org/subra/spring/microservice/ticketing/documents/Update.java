package org.subra.spring.microservice.ticketing.documents;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("serial")
@Getter
@Setter
@ToString

public class Update implements Serializable {
	private String message;
	private int reporterId;
	private LocalDateTime time;
}
