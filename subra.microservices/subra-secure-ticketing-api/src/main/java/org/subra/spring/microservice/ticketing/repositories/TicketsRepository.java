package org.subra.spring.microservice.ticketing.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.subra.spring.microservice.ticketing.documents.Ticket;

@Repository
public interface TicketsRepository extends MongoRepository<Ticket, Long> {

	Optional<Ticket> findByTicketNumber(String number);

}
