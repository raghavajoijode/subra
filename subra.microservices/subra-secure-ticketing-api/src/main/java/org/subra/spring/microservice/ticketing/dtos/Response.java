package org.subra.spring.microservice.ticketing.dtos;

import java.io.Serializable;

import org.subra.spring.microservice.ticketing.utils.ResponseMessage;
import org.subra.spring.microservice.ticketing.utils.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Response implements Serializable {
	private static final long serialVersionUID = 1L;
	private ResponseStatus status;
	private ResponseMessage message;
	private Object body;
}
