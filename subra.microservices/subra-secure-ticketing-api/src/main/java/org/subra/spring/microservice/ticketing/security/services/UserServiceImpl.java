package org.subra.spring.microservice.ticketing.security.services;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.subra.spring.microservice.ticketing.documents.User;
import org.subra.spring.microservice.ticketing.dtos.RegisterDto;
import org.subra.spring.microservice.ticketing.repositories.UsersRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserDetailsService {

	@Autowired private UsersRepository repository;
	@Autowired private JwtTokenService jwtTokenService;
	
	@Autowired private AuthenticationManager authenticationManager;
	@Autowired private PasswordEncoder passwordEncoder;

	@PostConstruct
	protected void active() {
		log.debug("TicketsService started succesfully");
	}

	// Loads user from repository using email
	public User loadUser(String email) {
		return repository.findByEmail(email).orElse(null);
	}

	public User authenticate(String email, String password, Map<String, Object> results) {
		try {
			Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
			results.put("token", jwtTokenService.generateTokenWithAuthentication(authentication));
			return loadUser(authentication.getName());
		} catch (BadCredentialsException be) {
			results.put("error", "User Credentials Mismatch");
			log.error("User Credentials Mismatch");
		}
		return null;
	}
	
	public boolean register(RegisterDto newUser, Map<String, Object> results) {
		if(validateNewUser(newUser) && StringUtils.isNotBlank(createUser(newUser).getEmail())) {
			log.trace("User created succesfully");
			return true;
		}
		log.error("User with given email already exists");
		results.put("error", "User with given email already exists");
		return false;
	}
	
	public List<User> getAllUsers(){
		return allUsers();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = loadUser(username);
		if (user == null || StringUtils.isBlank(user.getEmail())) {
			throw new UsernameNotFoundException("Cannot find email.");
		}
		return user;
	}
	
	private List<User> allUsers(){
		return repository.findAll();
	}
	
	private User createUser(RegisterDto newUser){
		return repository.save(User.create(newUser.getEmail(), passwordEncoder.encode(newUser.getPassword()), newUser.getName()));
	}
	
	private boolean validateNewUser(RegisterDto newUser){
		return validateEmail(newUser.getEmail()) && validatePassword(newUser.getPassword()) && loadUser(newUser.getEmail()) == null;
	}

	private boolean validateEmail(String email){
		return StringUtils.isNotBlank(email);
	}
	
	private boolean validatePassword(String password){
		return StringUtils.isNotBlank(password);
	}

}
