package org.subra.spring.microservice.ticketing.utils;

public enum ResponseStatus {
	FAILURE, SUCCESS;
}
