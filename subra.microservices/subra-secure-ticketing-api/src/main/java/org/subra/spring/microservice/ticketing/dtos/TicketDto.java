package org.subra.spring.microservice.ticketing.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class TicketDto {
	private String heading;
	private String description;
	private String status;
	private int reporterId;
	private int assigneeId;
}
