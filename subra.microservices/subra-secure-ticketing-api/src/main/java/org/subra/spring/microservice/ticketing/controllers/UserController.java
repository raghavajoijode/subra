package org.subra.spring.microservice.ticketing.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.subra.spring.microservice.ticketing.documents.User;
import org.subra.spring.microservice.ticketing.dtos.LoginDto;
import org.subra.spring.microservice.ticketing.dtos.RegisterDto;
import org.subra.spring.microservice.ticketing.dtos.Response;
import org.subra.spring.microservice.ticketing.security.services.UserServiceImpl;
import org.subra.spring.microservice.ticketing.utils.ResponseMessage;
import org.subra.spring.microservice.ticketing.utils.ResponseStatus;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/v1/users")
@Slf4j
public class UserController {

	@Autowired
	private UserServiceImpl userService;

	@PostMapping("/authenticate")
	public ResponseEntity<Response> authenticateUser(@RequestBody LoginDto login) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.authenticate");
		User user = userService.authenticate(login.getEmail(), login.getPassword(), results);
		if (user == null) {
			return buildResponse(HttpStatus.UNAUTHORIZED, true, ResponseMessage.OPERATION_EXECUTED, results);
		}
		results.put("user", user);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}

	@PostMapping("/register")
	public ResponseEntity<Response> createUser(@RequestBody RegisterDto newUser) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.register with email {} and not blank Password {}", newUser.getEmail(),
				newUser.getPassword());
		if (userService.register(newUser, results)) {
			results.put("user", newUser);
			return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
		}
		return buildResponse(HttpStatus.OK, true, ResponseMessage.OPERATION_EXECUTED, results);
	}

	@GetMapping
	public ResponseEntity<Response> getAllUsers() {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.getAllUsers");
		List<User> users = userService.getAllUsers();
		if (CollectionUtils.isEmpty(users)) {
			results.put("error", "No user available");
			return buildResponse(HttpStatus.OK, true, ResponseMessage.OPERATION_EXECUTED, results);
		}
		results.put("users", users);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}

	/*
	 * @PostMapping("/logout") public ResponseEntity
	 * logout(@NotNull @RequestHeader("Authorization") String logoutRequest) {
	 * String email = getEmailFromRequest(logoutRequest);
	 * 
	 * if (userService.logoutUser(email)) { Map<String, String> response = new
	 * HashMap<>(); response.put("status", "logged out"); return
	 * ResponseEntity.ok(response); }
	 * 
	 * return ResponseEntity.notFound().build(); }
	 * 
	 * @DeleteMapping("/delete") public ResponseEntity delete(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken,
	 * 
	 * @NotNull @Size(min = 8) @RequestBody String password) { String email =
	 * getEmailFromRequest(authorizationToken); Map results = new HashMap<String,
	 * String>(); if (!userService.deleteUser(email, password, results)) { return
	 * ResponseEntity.badRequest().body(results); } Map<String, String> response =
	 * new HashMap<>(); response.put("success", "deleted"); return
	 * ResponseEntity.ok(response); }
	 * 
	 * @PutMapping("/update-preferences") public ResponseEntity
	 * updateUserPreferences(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken,
	 * 
	 * @RequestBody Map<String, Object> userPreferences) { String email =
	 * getEmailFromRequest(authorizationToken); Map<String, Object> results = new
	 * HashMap<>(); if (!userService.updateUserPreferences(email, userPreferences,
	 * results)) { results.put("status", "fail"); return
	 * ResponseEntity.badRequest().body(results); } results.put("auth_token",
	 * tokenProvider.mintJWTHeader(email)); return ResponseEntity.ok(results); }
	 * 
	 * @GetMapping("/comment-report") public ResponseEntity getCommentReport(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken) { String email =
	 * getEmailFromRequest(authorizationToken); Map<String, Object> results = new
	 * HashMap<>(); User user = userService.loadUser(email); if (!user.isAdmin()) {
	 * results.put("status", "fail"); return
	 * ResponseEntity.status(401).body(results); }
	 * 
	 * results.put("auth_token", tokenProvider.mintJWTHeader(email));
	 * results.put("report", moviesService.mostActiveUsers()); return
	 * ResponseEntity.ok(results); }
	 */

	private ResponseEntity<Response> buildResponse(HttpStatus httpStatus, boolean isError, ResponseMessage message,
			Object body) {
		return ResponseEntity.status(httpStatus)
				.body(new Response(isError ? ResponseStatus.FAILURE : ResponseStatus.SUCCESS, message, body));
	}

}
