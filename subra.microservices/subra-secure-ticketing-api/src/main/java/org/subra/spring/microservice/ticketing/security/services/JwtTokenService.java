package org.subra.spring.microservice.ticketing.security.services;

import static java.util.Collections.emptyList;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.subra.spring.microservice.ticketing.documents.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Service
@Configuration
@Slf4j
public class JwtTokenService {

	@Value("${subra.jwt.expiryInMs}")
	private long jwtExpirationInMs;

	@Value(value = "${subra.jwt.secret}")
	private String jwtSecret;

	@Autowired
	private UserServiceImpl userService;

	@PostConstruct
	protected void activate() {
		log.info("JwtTokenService activated with secret {}", jwtSecret);
	}

	private static final String TOKEN_PREFIX = "Bearer";
	private static final String AUTHORIZATION_HEADER = "Authorization";

	/*
	 * Generated the complete Authorization Header Value
	 */
	private String generateAuthorizationHeaderValue(String username) {
		return TOKEN_PREFIX + " " + generateTokenWithUserName(username);
	}

	/*
	 * Adds authorization Header to the response object
	 */
	public void addAuthenticationHeader(HttpServletResponse res, String username) {
		res.addHeader(AUTHORIZATION_HEADER, generateAuthorizationHeaderValue(username));
	}

	/*
	 * Returns Authentication from request i.e Token
	 */
	public Authentication getAuthentication(HttpServletRequest request) {
		String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
		if (authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX)) {
			final String token = extractTokenFromAuthorizationHeader(authorizationHeader);
			UserDetails userDetails = null;
			try {
				userDetails = userService.loadUserByUsername(extractUserName(token));
			} catch (UsernameNotFoundException e) {
				log.error("Exception Occured", e);
			}
			return userDetails != null && validateToken(userDetails, token)
					? new UsernamePasswordAuthenticationToken(userDetails, null, emptyList())
					: null;
		}
		return null;
	}

	/*
	 * Returns JWT Token from Authentication
	 */
	public String generateTokenWithAuthentication(Authentication authentication) {
		User user = (User) authentication.getPrincipal();
		return generateTokenWithUserName(user.getEmail());
	}

	/*
	 * Returns JWT Token from Authentication
	 */
	public Boolean validateToken(UserDetails userDetails, String token) {
		return StringUtils.equals(extractUserName(token), userDetails.getUsername()) && !isTokenExpired(token);
	}

	private Boolean isTokenExpired(String token) {
		return extractAllClaims(token).getExpiration().before(new Date());
	}

	private String generateTokenWithUserName(String userName) {
		return Jwts.builder().setSubject(userName).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationInMs)).setIssuer("SubraAuthService")
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	private Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(extractTokenFromAuthorizationHeader(token))
				.getBody();
	}

	/*
	 * Return UserName i.e. Subject from Authentication Header value by trimming it
	 * to JWT Token
	 */
	public String extractUserName(String token) {
		try {
			return extractAllClaims(token).getSubject();
		} catch (Exception e) {
			log.error("Cannot validate user token `{}`: error thrown - {}", token, e.getMessage());
		}
		return null;
	}

	/*
	 * gets exact JWT token from Authentication header
	 */
	private String extractTokenFromAuthorizationHeader(String token) {
		return token.replace(TOKEN_PREFIX, "").trim();
	}
}
