package org.subra.spring.microservice.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubraRabbitMQApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraRabbitMQApplication.class, args);
	}
	
}
