package org.subra.spring.microservice.rabbitmq.example.dto.consumer;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.subra.spring.microservice.rabbitmq.example.dto.Person;

@Service
public class Consumer {

	@RabbitListener(queues = {"Mobile", "TV", "AC"})
	public void getMessage(Person p) {
		Timer timer = new Timer();
		
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
				System.out.println(p.getName());
			  }
			}, 1 * 60 * 1000);
	}
	
	/*
	 * @RabbitListener(queues = "Mobile") public void getMessage(byte[] message)
	 * throws IOException, ClassNotFoundException { ByteArrayInputStream bis = new
	 * ByteArrayInputStream(message); ObjectInput in = new ObjectInputStream(bis);
	 * Person p = (Person) in.readObject(); in.close(); bis.close();
	 * System.out.println(p.getName()); }
	 */
}
