package org.subra.spring.microservice.rabbitmq.example.dto.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.subra.common.api.dtos.EmailPayLoad;
import org.subra.spring.microservice.rabbitmq.example.dto.Person;

@RestController
@RequestMapping("/api/v1/rabbitmq")
public class Controller {
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@GetMapping("/test/{name}")
	public String testAPI(@PathVariable("name") String name) {
		Person p1 = new Person(1, name + "Mobile");
		rabbitTemplate.convertAndSend("Mobile", p1);
		return "Success";
	}
	
	@GetMapping("/simple/{name}")
	public String sendEmailAPI(@PathVariable("name") String name) {
		EmailPayLoad p = new EmailPayLoad();
		p.setToAddress(name);
		p.setSubject("Frome rabbit service");
		p.setBody("hello");
		rabbitTemplate.convertAndSend("SimpleEmail", p);
		return "Success";
	}
	
	@GetMapping("/direct/{name}")
	public String testDirectAPI(@PathVariable("name") String name) {
		Person p2 = new Person(1, name + "Direct Exchange");
		rabbitTemplate.convertAndSend("Direct-Exchange", "mobile", p2);
		return "Success";
	}
	@GetMapping("/fanout/{name}")
	public String testFanoutAPI(@PathVariable("name") String name) {
		Person p3 = new Person(1, name + "Fanout Exchange");
		rabbitTemplate.convertAndSend("Fanout-Exchange", "", p3);
		return "Success";
	}
	@GetMapping("/topic/{name}")
	public String testTopicAPI(@PathVariable("name") String name) {
		Person p4 = new Person(1, name + "Topic-Exchange");
		rabbitTemplate.convertAndSend("Topic-Exchange", "tv.mobile.ac", p4);
		return "Success";
	}
	
	/*
	 * @GetMapping("/test/{name}") public String testAPI(@PathVariable("name")
	 * String name) throws IOException { Person p = new Person(1L, name);
	 * 
	 * ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out =
	 * new ObjectOutputStream(bos); out.writeObject(p); out.flush(); out.close();
	 * 
	 * byte[] byteMessage = bos.toByteArray(); bos.close();
	 * 
	 * Message message = MessageBuilder.withBody(byteMessage) .setHeader("item1",
	 * "mobile") .setHeader("item2", "television").build();
	 * 
	 * rabbitTemplate.send("Headers-Exchange", "", message);
	 * 
	 * return "Success"; }
	 */
}
