package org.subra.spring.microservice.ticketing.services.impl;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.subra.common.api.dtos.Response;
import org.subra.common.api.dtos.TicketDto;
import org.subra.common.api.dtos.UpdateDto;
import org.subra.common.api.utils.ResponseMessage;
import org.subra.common.api.utils.ResponseStatus;
import org.subra.common.api.utils.StatusType;
import org.subra.spring.microservice.common.api.entities.Ticket;
import org.subra.spring.microservice.common.api.repos.SubraTicketsRepository;
import org.subra.spring.microservice.ticketing.services.TicketsService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TicketsServiceImpl implements TicketsService {

	@Autowired SubraTicketsRepository repository;

	@PostConstruct
	protected void active() {
		log.debug("TicketsService started succesfully");
	}

	@Override
	@CachePut(value = "findTicketById", key = "#id")
	@CacheEvict(cacheNames = { "findAllTickets" }, allEntries = true)
	public Response createTicket(TicketDto newTicket, long id) {
		Ticket ticket = this.copyProperties(new Ticket(), newTicket);
		createTicket(ticket, id);
		return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, repository.save(ticket));
	}

	@Override
	@Cacheable(value = "findAllTickets", key = "'allTickets'")
	public Response findAllTickets() {
		log.info("Finding all tickets {}", getAllTickets().size());
		return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, getAllTickets());
	}

	@Override
	@Cacheable(value = "findTicketById", key = "#id")
	public Response findTicketById(long id) {
		log.debug("finding ticket with id {}", id);
		Optional<Ticket> ticket = repository.findById(id);
		if (ticket.isPresent())
			return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, ticket.get());
		return new Response(ResponseStatus.FAILURE, ResponseMessage.NOT_FOUND, null);
	}

	@Override
	@Cacheable(value = "findTicketByNumber", key = "#number")
	public Response findTicketByTicketNumber(String number) {
		log.debug("finding ticket with number {}", number);
		Optional<Ticket> ticket = repository.findByTicketNumber(number);
		if (ticket.isPresent())
			return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, ticket.get());
		return new Response(ResponseStatus.FAILURE, ResponseMessage.NOT_FOUND, null);
	}

	@Override
	@CachePut(value = "findTicketById", key = "#updateDto.id")
	@CacheEvict(cacheNames = { "findAllTickets" }, allEntries = true)
	public Response updateTicket(UpdateDto updateDto) {
		log.debug("Updating");
		Optional<Ticket> optional = repository.findById(updateDto.getId());
		Ticket ticket = this.copyProperties(optional.orElse(new Ticket()), updateDto);
		if (optional.isPresent()) {
			updateTicket(updateDto, ticket);
			return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, repository.save(ticket));
		}
		return new Response(ResponseStatus.FAILURE, ResponseMessage.NOT_FOUND, null);
	}

	@Override
	@CacheEvict(value = "findTicketById", key = "#id")
	public Response deleteTicketById(long id) {
		if (repository.findById(id).isPresent())
			repository.deleteById(id);
		else
			return new Response(ResponseStatus.FAILURE, ResponseMessage.NOT_FOUND, null);
		return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, null);
	}

	@Override
	@CacheEvict(value = { "findTicketById", "findAllTickets" }, allEntries = true)
	public Response deleteAllTickets() {
		if (repository.findAll().isEmpty())
			return new Response(ResponseStatus.FAILURE, ResponseMessage.EMPTY_REPOSITORY, null);
		repository.deleteAll();
		return new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, null);
	}

	@Override
	public long generateTicketId() {
		return Math.incrementExact(getAllTickets().size());
	}

	private <T, P> T copyProperties(T target, P source, String... ignoreProperties) {
		BeanUtils.copyProperties(source, target, ignoreProperties);
		return target;
	}

	private void updateTicket(UpdateDto updateDto, Ticket ticket) {
		List<UpdateDto> updates = new LinkedList<>(CollectionUtils.emptyIfNull(ticket.getUpdates()));
		updateDto.setTime(LocalDateTime.now());
		updates.add(copyProperties(new UpdateDto(), updateDto));
		ticket.setUpdates(updates);
		ticket.setUpdatedDate(LocalDateTime.now());
		ticket.setStatus(StatusType.IN_PROGRESS.value());
	}

	private void createTicket(Ticket ticket, long id) {
		ticket.setTicketNumber(generateTicketNumber());
		ticket.setId(id);
		ticket.setStatus(StatusType.OPEN.value());
		ticket.setCreatedDate(LocalDateTime.now());
	}

	private String generateTicketNumber() {
		return String.format("%s%s%s", RandomStringUtils.randomAlphabetic(3).toUpperCase(),
				RandomStringUtils.randomNumeric(3), generateTicketId()).toUpperCase();
	}

	private List<Ticket> getAllTickets() {
		return repository.findAll(Sort.by("createdDate"));
	}

}
