package org.subra.spring.microservice.ticketing.services;

import org.subra.common.api.dtos.Response;
import org.subra.common.api.dtos.TicketDto;
import org.subra.common.api.dtos.UpdateDto;

public interface TicketsService {

	Response createTicket(TicketDto newTicket, long id);

	Response findAllTickets();

	Response findTicketById(long id);
	
	Response findTicketByTicketNumber(String number);

	Response updateTicket(UpdateDto update);

	Response deleteTicketById(long id);

	Response deleteAllTickets();

	long generateTicketId();

}
