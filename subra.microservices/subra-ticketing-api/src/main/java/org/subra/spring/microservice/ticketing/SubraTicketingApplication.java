package org.subra.spring.microservice.ticketing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories("org.subra.spring.microservice.common.api.repos")
public class SubraTicketingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraTicketingApplication.class, args);
	}
	
}
