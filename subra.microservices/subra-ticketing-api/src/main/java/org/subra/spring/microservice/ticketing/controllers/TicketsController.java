package org.subra.spring.microservice.ticketing.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.subra.common.api.dtos.Response;
import org.subra.common.api.dtos.TicketDto;
import org.subra.common.api.dtos.UpdateDto;
import org.subra.common.api.utils.ResponseMessage;
import org.subra.common.api.utils.ResponseStatus;
import org.subra.spring.microservice.ticketing.services.TicketsService;

@RestController
@RequestMapping(path = "/api/v1/tickets")
public class TicketsController {

	@Autowired
	private TicketsService service;

	@PostMapping("/create")
	public ResponseEntity<Response> createTicket(@RequestBody TicketDto newTicket) {
		return buildSuccess2(service.createTicket(newTicket, service.generateTicketId()));
	}

	@GetMapping
	public ResponseEntity<Response> findAllTickets() {
		return buildSuccess2(service.findAllTickets());
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<Response> findTicketById(@PathVariable long id) {
		return buildSuccess2(service.findTicketById(id));
	}

	@GetMapping(path = "/number/{number}")
	public ResponseEntity<Response> findTicketByTicketNumber(@PathVariable String number) {
		return buildSuccess2(service.findTicketByTicketNumber(number));
	}

	@PutMapping(path = "/update")
	public ResponseEntity<Response> updateTicket(@RequestBody UpdateDto update) {
		return buildSuccess2(service.updateTicket(update));
	}

	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Response> deleteTicketById(@PathVariable long id) {
		return buildSuccess2(service.deleteTicketById(id));
	}

	@DeleteMapping(path = "/delete")
	public ResponseEntity<Response> deleteAllTickets() {
		return buildSuccess2(service.deleteAllTickets());
	}

	private ResponseEntity<Response> buildSuccess2(Response response) {
		new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, null);
		return ResponseEntity.ok(response);
	}

	private ResponseEntity<Response> buildSuccess(Object body) {
		return ResponseEntity.ok(new Response(ResponseStatus.SUCCESS, ResponseMessage.OPERATION_EXECUTED, body));
	}

	private ResponseEntity<Response> buildError(Object body) {
		return ResponseEntity.ok(new Response(ResponseStatus.FAILURE, ResponseMessage.OPERATION_EXECUTED, body));
	}

}
