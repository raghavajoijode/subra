package org.subra.spring.microservice.ticketing.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.subra.spring.microservice.common.api.security.filters.SubraCommonAuthenticationFilter;
import org.subra.spring.microservice.common.api.security.filters.SubraCommonJWTAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@ComponentScan("org.subra.spring.microservice.common.api.security.filters")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SubraCommonAuthenticationFilter authenticationFilter;
	@Autowired
	private SubraCommonJWTAuthenticationEntryPoint authenticationEntryPoint;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and() // For exception Handling
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() // Make sure no sessions are stored
				.authorizeRequests().anyRequest().authenticated().and() // Authenticate all request
				.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class); // Go through our filter where authentication is set
	}

}
