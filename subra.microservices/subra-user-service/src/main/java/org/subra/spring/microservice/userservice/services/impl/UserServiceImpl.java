package org.subra.spring.microservice.userservice.services.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.subra.common.api.dtos.account.NewUser;
import org.subra.spring.microservice.common.api.entities.UserEntity;
import org.subra.spring.microservice.common.api.repos.SubraUsersRepository;
import org.subra.spring.microservice.common.api.services.SubraJwtTokenService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Import(SubraJwtTokenService.class)
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private SubraUsersRepository repository;
	@Autowired
	private SubraJwtTokenService jwtTokenService;

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@PostConstruct
	protected void active() {
		log.debug("UserServiceImpl started succesfully");
	}

	// Loads user from repository using email
	public UserEntity loadUser(final String email) {
		return repository.findByEmail(email).orElse(null);
	}

	// Loads user from repository using accountId
	public UserEntity loadUserById(final String accountId) {
		return repository.findByAccountId(accountId).orElse(null);
	}
	
	// delete user from repository using accountId
	public boolean deleteUserById(final String accountId) {
		if (repository.findByAccountId(accountId).isPresent()) {
			repository.deleteByAccountId(accountId);
			return true;
		}
		return false;
	}

	public UserEntity authenticate(String email, String password, Map<String, Object> results) {
		try {
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(email, password));
			results.put("token", jwtTokenService.generateTokenWithAuthentication(authentication));
			return loadUser(authentication.getName());
		} catch (BadCredentialsException be) {
			results.put("error", "User Credentials Mismatch");
			log.error("User Credentials Mismatch");
		}
		return null;
	}

	public boolean register(NewUser newUser, Map<String, Object> results) {
		if (validateNewUser(newUser)) {
			try {
				results.put("user", createUser(newUser));
			} catch (Exception e) {
				results.put("error", "Unable to create user - please contact customer care");
				return false;
			}
			log.trace("User created succesfully");
			return true;
		}
		log.error("User with given email already exists");
		results.put("error", "User with given email already exists");
		return false;
	}

	public List<UserEntity> getAllUsers() {
		return allUsers();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = loadUser(username);
		if (userEntity == null || StringUtils.isBlank(userEntity.getEmail())) {
			throw new UsernameNotFoundException("Cannot find email.");
		}
		return userEntity;
	}

	private List<UserEntity> allUsers() {
		return repository.findAll();
	}
	
	private String generateUniqueUserId() {
		int maxUniqueId = allUsers().stream().map(u -> u.getAccountId()).filter(StringUtils::isNotBlank).mapToInt(Integer::parseInt).distinct().max().orElse(100000);
		return String.valueOf(Math.incrementExact(maxUniqueId));
	}

	private UserEntity createUser(NewUser newUser) throws Exception {
		String id = generateUniqueUserId();
		if(loadUserById(id) != null)
			throw new Exception("Error Creating User");
		return repository.save(UserEntity.create(newUser.getEmail(), passwordEncoder.encode(newUser.getPassword()),
				newUser.getName(), generateUniqueUserId()));
	}

	private boolean validateNewUser(NewUser newUser) {
		return validateEmail(newUser.getEmail()) && validatePassword(newUser.getPassword())
				&& loadUser(newUser.getEmail()) == null;
	}

	private boolean validateEmail(String email) {
		return StringUtils.isNotBlank(email);
	}

	private boolean validatePassword(String password) {
		return StringUtils.isNotBlank(password);
	}

}
