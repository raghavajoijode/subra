package org.subra.spring.microservice.userservice.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.subra.common.api.dtos.Response;
import org.subra.common.api.dtos.account.Login;
import org.subra.common.api.dtos.account.NewUser;
import org.subra.common.api.dtos.account.User;
import org.subra.common.api.utils.ResponseMessage;
import org.subra.common.api.utils.ResponseStatus;
import org.subra.spring.microservice.common.api.entities.UserEntity;
import org.subra.spring.microservice.userservice.services.impl.UserServiceImpl;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/v1/users")
@Slf4j
@RefreshScope
public class UserController {

	
	//TODO: add update user
	//TODO: cache for user service
	//TODO: add spring cloud config service
	
	@Value("${welcome.messageone:def message 1}")
	String welcomeMessageOne;
	
	@Value("${welcome.messagetwo:def message 2}")
	String welcomeMessageTwo;
	
	@Autowired
	private UserServiceImpl userService;

	@GetMapping("/test")
	public String testApi() {
		log.trace("Calling userService.testApi");
		return welcomeMessageOne + " _____ " + welcomeMessageTwo;
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<Response> authenticateUser(@RequestBody Login login) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.authenticate");
		User user = userService.authenticate(login.getEmail(), login.getPassword(), results);
		if (user == null) {
			return buildResponse(HttpStatus.UNAUTHORIZED, true, ResponseMessage.OPERATION_EXECUTED, results);
		}
		results.put("user", user);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}

	@PostMapping("/register")
	public ResponseEntity<Response> createUser(@RequestBody NewUser newUser) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.register with email {} and not blank Password {}", newUser.getEmail(),
				newUser.getPassword());
		if (userService.register(newUser, results)) {
			return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
		}
		return buildResponse(HttpStatus.OK, true, ResponseMessage.ALREADY_AVAILABLE, results);
	}
	
	@GetMapping("/validate")
	public ResponseEntity<Response> validateUser(@RequestParam String email) {
		Map<String, Object> results = new HashMap<>();
		results.put("isExisting", false);
		if(null != userService.loadUser(email))
			results.put("isExisting", true);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}

	@GetMapping
	public ResponseEntity<Response> getAllUsers() {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.getAllUsers");
		List<UserEntity> userEntities = userService.getAllUsers();
		if (CollectionUtils.isEmpty(userEntities)) {
			results.put("error", "No user available");
			return buildResponse(HttpStatus.OK, true, ResponseMessage.EMPTY_REPOSITORY, results);
		}
		results.put("users", userEntities);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Response> getUserByID(@PathVariable final String id) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.loadUserById");
		User user = userService.loadUserById(id);
		if (user == null) {
			return buildResponse(HttpStatus.UNAUTHORIZED, true, ResponseMessage.OPERATION_EXECUTED, results);
		}
		results.put("user", user);
		return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Response> deleteUserByID(@PathVariable final String id) {
		Map<String, Object> results = new HashMap<>();
		log.trace("Calling userService.loadUserById");
		if (userService.deleteUserById(id)) {
			results.put("deleted", true);
			return buildResponse(HttpStatus.OK, false, ResponseMessage.OPERATION_EXECUTED, results);
		}
		results.put("deleted", false);
		return buildResponse(HttpStatus.OK, true, ResponseMessage.OPERATION_EXECUTED, results);
	}

	/*
	 * @PostMapping("/logout") public ResponseEntity
	 * logout(@NotNull @RequestHeader("Authorization") String logoutRequest) {
	 * String email = getEmailFromRequest(logoutRequest);
	 * 
	 * if (userService.logoutUser(email)) { Map<String, String> response = new
	 * HashMap<>(); response.put("status", "logged out"); return
	 * ResponseEntity.ok(response); }
	 * 
	 * return ResponseEntity.notFound().build(); }
	 * 
	 * @DeleteMapping("/delete") public ResponseEntity delete(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken,
	 * 
	 * @NotNull @Size(min = 8) @RequestBody String password) { String email =
	 * getEmailFromRequest(authorizationToken); Map results = new HashMap<String,
	 * String>(); if (!userService.deleteUser(email, password, results)) { return
	 * ResponseEntity.badRequest().body(results); } Map<String, String> response =
	 * new HashMap<>(); response.put("success", "deleted"); return
	 * ResponseEntity.ok(response); }
	 * 
	 * @PutMapping("/update-preferences") public ResponseEntity
	 * updateUserPreferences(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken,
	 * 
	 * @RequestBody Map<String, Object> userPreferences) { String email =
	 * getEmailFromRequest(authorizationToken); Map<String, Object> results = new
	 * HashMap<>(); if (!userService.updateUserPreferences(email, userPreferences,
	 * results)) { results.put("status", "fail"); return
	 * ResponseEntity.badRequest().body(results); } results.put("auth_token",
	 * tokenProvider.mintJWTHeader(email)); return ResponseEntity.ok(results); }
	 * 
	 * @GetMapping("/comment-report") public ResponseEntity getCommentReport(
	 * 
	 * @RequestHeader("Authorization") String authorizationToken) { String email =
	 * getEmailFromRequest(authorizationToken); Map<String, Object> results = new
	 * HashMap<>(); User user = userService.loadUser(email); if (!user.isAdmin()) {
	 * results.put("status", "fail"); return
	 * ResponseEntity.status(401).body(results); }
	 * 
	 * results.put("auth_token", tokenProvider.mintJWTHeader(email));
	 * results.put("report", moviesService.mostActiveUsers()); return
	 * ResponseEntity.ok(results); }
	 */

	private ResponseEntity<Response> buildResponse(HttpStatus httpStatus, boolean isError, ResponseMessage message,
			Object body) {
		return ResponseEntity.status(httpStatus)
				.body(new Response(isError ? ResponseStatus.FAILURE : ResponseStatus.SUCCESS, message, body));
	}

}
