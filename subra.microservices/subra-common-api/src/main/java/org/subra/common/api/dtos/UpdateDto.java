package org.subra.common.api.dtos;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UpdateDto {
	private static final long serialVersionUID = 1L;
	private long id;
	private String message;
	private int reporterId;
	private LocalDateTime time;
}
