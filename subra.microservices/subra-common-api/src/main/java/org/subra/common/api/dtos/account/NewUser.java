package org.subra.common.api.dtos.account;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class NewUser implements Serializable {
	private static final long serialVersionUID = -6827435358649484483L;
	private String email;
	private String password;
	private String name;
}
