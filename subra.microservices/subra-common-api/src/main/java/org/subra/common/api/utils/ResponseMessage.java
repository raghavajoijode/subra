package org.subra.common.api.utils;

public enum ResponseMessage {
	EMPTY_REPOSITORY, NOT_FOUND, ALREADY_AVAILABLE, OPERATION_EXECUTED;
}
