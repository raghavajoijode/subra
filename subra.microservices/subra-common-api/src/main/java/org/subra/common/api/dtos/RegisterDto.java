package org.subra.common.api.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegisterDto {

	private String email;
	private String password;
	private String name;
}
