package org.subra.common.api.dtos;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TicketDto {
	private long id;
	private String ticketNumber;
	private String heading;
	private String description;
	private String status;
	private List<UpdateDto> updates;
	private int reporterId;
	private int assigneeId;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;
}
