package org.subra.common.api.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String email;
	private String name;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;

}
