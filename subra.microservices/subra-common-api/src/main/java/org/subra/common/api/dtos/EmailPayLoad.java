package org.subra.common.api.dtos;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmailPayLoad implements Serializable {

	private static final long serialVersionUID = 1615385120899678724L;
	private String toAddress;
	private String subject;
	private String body;
}
