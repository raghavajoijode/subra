package org.subra.common.api.dtos.account;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User implements Serializable {
	private static final long serialVersionUID = 5074899938394040725L;
	private String email;
	private String accountId;
	private String name;
	private boolean isVerified;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;
}
