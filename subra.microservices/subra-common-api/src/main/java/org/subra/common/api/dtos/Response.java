package org.subra.common.api.dtos;

import java.io.Serializable;

import org.subra.common.api.utils.ResponseMessage;
import org.subra.common.api.utils.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Response implements Serializable {
	private static final long serialVersionUID = 1L;
	private ResponseStatus status;
	private ResponseMessage message;
	private Object body;
}
