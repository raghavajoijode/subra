const express = require('express');

const app = express();

const PORT = 5000;

//const HOST = "0.0.0.0";

app.get('/', (req, res) => {

    res.send('Hello world')
});

app.listen(PORT, () => console.log(`Example app listening at http://localhost:${PORT}`))