const express = require("express");

const HOST = "0.0.0.0";
const PORT = 80;

const app = express();

app.get("/", (req, res) =>
    res.json({
        players: ["Raju", "Somu", "Kiram", "Krish", "Raghava"]
    })
);

app.listen(PORT, HOST, () => console.log(`Starting at http://${HOST}:${PORT}`))