package org.subra.aem.archive.core.util;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(enabled = true, immediate = true, ds = true, metatype = true, createPid = true,
        label = "CDN Transformer Factory", description = "CDN Transformer Factory")
@Service
@Property(name="pipeline.type", value="demorewriter", propertyPrivate=false)
public class NexusLinkWriter implements org.apache.sling.rewriter.TransformerFactory {
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(NexusLinkWriter.class);
    @Reference
    SlingSettingsService slingServiceNew;
    
    public NexusLinkWriter() {
        LOG.info("inside the constructor");
    }
    private static final int RANKING = 999;

    @Reference
    private SlingRepository repository;
 
    @Property(intValue = RANKING)
    private static final String SERVICE_RANKING = "service.ranking";

    public CDNTransformer createTransformer() {
        return new CDNTransformer(this,slingServiceNew);
    }
    public SlingRepository getRepository() {
        return repository;
    }

    protected void bindRepository(SlingRepository newRepository) {
        this.repository = newRepository;
    }

    protected void unbindRepository(SlingRepository newRepository) {
        this.repository = null;
    }
    @Activate
    protected void activate(ComponentContext componentContext) {
        LOG.info("Entry into activate method");
      
    }
 
    @Modified
    protected void modified(ComponentContext newComponentContext) {
        LOG.info("Entry into BMSTransformerFactory");
        this.activate(newComponentContext);
        LOG.info("Exit from BMSTransformerFactory");
    }

}
