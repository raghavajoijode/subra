package org.subra.aem.archive.core.util;

import java.io.IOException;
import org.apache.cocoon.xml.sax.AttributesImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subra.aem.archive.core.constants.Defaults;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import java.util.Set;
import java.util.regex.*;  


public class CDNTransformer extends org.apache.cocoon.xml.sax.AbstractSAXPipe implements
        org.apache.sling.rewriter.Transformer {

    private static final Logger LOG = LoggerFactory.getLogger(CDNTransformer.class);

    @Reference
    private final NexusLinkWriter factory;  
    @Reference
    private final SlingSettingsService slingService;
    
    public CDNTransformer(NexusLinkWriter newFactory, SlingSettingsService slingServiceNew) {
        super();
        this.factory = newFactory;
        this.slingService= slingServiceNew;
    }
    public void init(ProcessingContext processingContext, ProcessingComponentConfiguration config) throws IOException {
        
    }
    @Override
    public void startElement(String uri, String loc, String raw, Attributes attributes) throws SAXException {
		AttributesImpl attrs = new AttributesImpl(attributes);
        Set<String> modes = slingService.getRunModes();
        Boolean isPublish = modes.contains("publish");
        if(isPublish){
			
      // (case-study|latest-thinking|offerings|solutions|segments)
            for (int i = 0; i < attrs.getLength(); i++) {
                String name = attrs.getLocalName(i);  
                if(name.equals("href"))
                {
					String blogRegex = "/blogs/([^/]+)/(.+)";
                    String url = attrs.getValue(i);
					if(!(url.contains("/content/dam"))){
                    url = url.replace(".html", "/");
                    if(url.contains("case-study"))
                    {
                    url = url.replace("/case-study/", "/");
                    }else if(url.contains("latest-thinking"))
                    {
                    url = url.replace("/latest-thinking/", "/");
					}else if(url.contains("offerings"))
                    {
                    url = url.replace("/offerings/", "/");
                    }else if(url.contains("solutions"))
                    {
                    url = url.replace("/solutions/", "/");
                    }else if(url.contains("segments"))
                    {
                    url = url.replace("/segments/", "/");
                    }
					
					if(url.contains("service-lines"))
                    {
                    url = url.replace("/service-lines/", "/");
                    }else if(url.contains("industries"))
                    {
                    url = url.replace("/industries/", "/");
					}/*else if(Pattern.matches(blogRegex, url))
                    {
                    url = url.replaceAll(blogRegex , "/blogs/$2");
                    }*/
					if(url.contains("/en/"))
                    {
                    url = url.replace("/en/", "/");
                    }
                    attrs.setValue(i, url);                    		    
					} 
				}
            }
        }
        super.startElement(uri, loc, raw, attrs);
        
    }
    public void dispose() {
        LOG.info("Entry into dispose method");
    }
	@Override
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void startPrefixMapping(String prefix, String uri) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void processingInstruction(String target, String data) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setContentHandler(ContentHandler arg0) {
		// TODO Auto-generated method stub
		
	}
}