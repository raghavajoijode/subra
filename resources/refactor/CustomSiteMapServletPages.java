package org.subra.aem.archive.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.servlet.ServletException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.AssetReferenceSearch;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.PageManager;


@SuppressWarnings("serial")
@Component(metatype = true, label = "Customized - Site Map Servlet - Web ", description = "Constomized Site Map Servlet ", configurationFactory = true)
@Service(value = javax.servlet.Servlet.class)
@Properties({ @Property(name = "sling.servlet.resourceTypes", unbounded = PropertyUnbounded.ARRAY),
		@Property(name = "sling.servlet.selectors", value = "sitemap"),
		@Property(name = "sling.servlet.extensions", value = "xml"),
		@Property(name = "sling.servlet.methods", value = "GET")

})
public class CustomSiteMapServletPages extends SlingSafeMethodsServlet {
	private static final Logger LOG = LoggerFactory.getLogger(CustomSiteMapServletPages.class);

	private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");

	private static final boolean DEFAULT_INCLUDE_LAST_MODIFIED = false;

	private static final String DEFAULT_EXTERNALIZER_DOMAIN = "publish";

	@Property(value = DEFAULT_EXTERNALIZER_DOMAIN, label = "Externalizer Domain", description = "Must correspond to a configuration of the Externalizer component.")
	private static final String PROP_EXTERNALIZER_DOMAIN = "externalizer.domain";

	@Property(label = "Externalizer Domain", unbounded = PropertyUnbounded.ARRAY, description = "Must correspond to a configuration of the Externalizer component.")
	private static final String PROP_EXTERNALIZER_DOMAIN_ARRAY = "externalizer.properties";

	@Property(boolValue = DEFAULT_INCLUDE_LAST_MODIFIED, label = "Include Last Modified", description = "If true, the last modified value will be included in the sitemap.")
	private static final String PROP_INCLUDE_LAST_MODIFIED = "include.lastmod";

	@Property(label = "Change Frequency Properties", unbounded = PropertyUnbounded.ARRAY, description = "The set of JCR property names which will contain the change frequency value.")
	private static final String PROP_CHANGE_FREQUENCY_PROPERTIES = "changefreq.properties";

	@Property(label = "Priority Properties", unbounded = PropertyUnbounded.ARRAY, description = "The set of JCR property names which will contain the priority value.")
	private static final String PROP_PRIORITY_PROPERTIES = "priority.properties";

	@Property(label = "DAM Folder Property", description = "The JCR property name which will contain DAM folders to include in the sitemap.")
	private static final String PROP_DAM_ASSETS_PROPERTY = "damassets.property";

	@Property(label = "DAM Asset MIME Types", unbounded = PropertyUnbounded.ARRAY, description = "MIME types allowed for DAM assets.")
	private static final String PROP_DAM_ASSETS_TYPES = "damassets.types";

	private static final String NS = "https://www.sitemaps.org/schemas/sitemap/0.9";
	private static final String NS_IMAGE = "http://www.google.com/schemas/sitemap-image/1.1";
	private static final String NS_VIDEO ="http://www.google.com/schemas/sitemap-video/1.1";

	@Reference
	private Externalizer externalizer;

	private String externalizerDomain;

	private String[] externalizerDomainArray;

	private boolean includeLastModified;

	private String[] changefreqProperties;

	private String[] priorityProperties;

	private String damAssetProperty;

	private List<String> damAssetTypes;
	
	

	@Activate
	protected void activate(Map<String, Object> properties) {
		this.externalizerDomain = PropertiesUtil.toString(properties.get(PROP_EXTERNALIZER_DOMAIN),
				DEFAULT_EXTERNALIZER_DOMAIN);
		this.externalizerDomainArray = PropertiesUtil.toStringArray(properties.get(PROP_EXTERNALIZER_DOMAIN_ARRAY),
				new String[0]);
		this.includeLastModified = PropertiesUtil.toBoolean(properties.get(PROP_INCLUDE_LAST_MODIFIED),
				DEFAULT_INCLUDE_LAST_MODIFIED);
		this.changefreqProperties = PropertiesUtil.toStringArray(properties.get(PROP_CHANGE_FREQUENCY_PROPERTIES),
				new String[0]);
		this.priorityProperties = PropertiesUtil.toStringArray(properties.get(PROP_PRIORITY_PROPERTIES), new String[0]);
		this.damAssetProperty = "/content/dam/demo";//PropertiesUtil.toString(properties.get(PROP_DAM_ASSETS_PROPERTY), "");
		this.damAssetTypes = Arrays
				.asList(PropertiesUtil.toStringArray(properties.get(PROP_DAM_ASSETS_TYPES), new String[0]));
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/xml");
		response.setCharacterEncoding("UTF-8");
		ResourceResolver resourceResolver = request.getResourceResolver();
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		Page page = pageManager.getContainingPage(request.getResource());

		String resourcePath = page.getPath();
		String parseResource[] = resourcePath.split("/");
		String externalizerBrandName = parseResource[2];
		for (int i = 0; i < externalizerDomainArray.length; i++) {
			if (externalizerBrandName.equals(externalizerDomainArray[i])) {
				this.externalizerDomain = externalizerDomainArray[i];
				break;
			}
		}

		XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
		try {
			XMLStreamWriter stream = outputFactory.createXMLStreamWriter(response.getWriter());
			stream.writeStartDocument("1.0");

			stream.writeStartElement("", "urlset", NS);
			stream.writeNamespace("", NS);
			stream.writeNamespace("image", NS_IMAGE);
			stream.writeNamespace("video", NS_VIDEO);

			// first do the current page
			write(page, stream, resourceResolver, request);

			for (Iterator<Page> children = page.listChildren(new PageFilter(), true); children.hasNext();) {
				if (!(page.getName().equals("industries") || page.getName().equals("service-lines")
						|| page.getName().equals("case-study") || page.getName().equals("latest-thinking")
						|| page.getName().equals("offerings") || page.getName().equals("solutions")
						|| page.getName().equals("segments"))) {
				write(children.next(), stream, resourceResolver, request);
				}
			}
			if (damAssetTypes.size() > 0 && damAssetProperty!=null) {
				writeAssets(stream, resourceResolver.getResource(damAssetProperty), resourceResolver);
			}

			stream.writeEndElement();
			stream.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	

	private void write(Page page, XMLStreamWriter stream, ResourceResolver resolver, SlingHttpServletRequest request)
			throws XMLStreamException {
		
		if (!(page.getName().equals("industries") || page.getName().equals("service-lines")
				|| page.getName().equals("case-study") || page.getName().equals("latest-thinking")
				|| page.getName().equals("offerings") || page.getName().equals("solutions")
				|| page.getName().equals("segments"))) {
			
		stream.writeStartElement(NS, "url");
		String loc = externalizer.externalLink(resolver, externalizerDomain, String.format("%s/", page.getPath()));
		if (loc.contains("case-study")) {
			loc = loc.replace("/case-study/", "/");
		} else if (loc.contains("latest-thinking")) {
			loc = loc.replace("/latest-thinking/", "/");
		} else if (loc.contains("offerings")) {
			loc = loc.replace("/offerings/", "/");
		} else if (loc.contains("solutions")) {
			loc = loc.replace("/solutions/", "/");
		} else if (loc.contains("segments")) {
			loc = loc.replace("/segments/", "/");
		} else if (loc.contains("industries")) {
			loc = loc.replace("/industries/", "/");
		} else if (loc.contains("service-lines")) {
			loc = loc.replace("/service-lines/", "/");
		}
		loc=loc.replace("http://", "https://");
		writeElement(stream, "loc", loc);
		if (includeLastModified) {
			Calendar cal = page.getLastModified();
			if (cal != null) {
				writeElement(stream, "lastmod", DATE_FORMAT.format(cal));
			}
			
		}
		

		final ValueMap properties = page.getProperties();
		writeFirstPropertyValue(stream, "changefreq", changefreqProperties, properties);
		writeFirstPropertyValue(stream, "priority", priorityProperties, properties);

		stream.writeEndElement();
		}
	}

	private void writeAsset(Asset asset, XMLStreamWriter stream, ResourceResolver resolver) throws XMLStreamException {
		stream.writeStartElement(NS, "url");
		String loc = externalizer.externalLink(resolver, externalizerDomain, asset.getPath());
		writeElement(stream, "loc", loc);
		if (includeLastModified) {
			long lastModified = asset.getLastModified();
			if (lastModified > 0) {
				writeElement(stream, "lastmod", DATE_FORMAT.format(lastModified));
			}
		}

		Resource contentResource = asset.adaptTo(Resource.class).getChild("jcr:content");
		if (contentResource != null) {
			final ValueMap properties = contentResource.getValueMap();
			writeFirstPropertyValue(stream, "changefreq", changefreqProperties, properties);
			writeFirstPropertyValue(stream, "priority", priorityProperties, properties);
		}

		stream.writeEndElement();
	}

	private void writeAssets(final XMLStreamWriter stream, final Resource assetFolder, final ResourceResolver resolver)
			throws XMLStreamException {

		for (Iterator<Resource> children = assetFolder.listChildren(); children.hasNext();) {
			Resource assetFolderChild = children.next();
			if (assetFolderChild.isResourceType("dam:Asset")) {
				LOG.info("Enters before aaa");
				Asset asset = assetFolderChild.adaptTo(Asset.class);
				LOG.info("Image of type DAM:ASSET");
				if (damAssetTypes.contains(asset.getMimeType())) {
					writeAsset(asset, stream, resolver);
				}
			} else {
				LOG.info("Image not of type DAM:ASSET");
				writeAssets(stream, assetFolderChild, resolver);
			}
		}
	}

	private void writeFirstPropertyValue(final XMLStreamWriter stream, final String elementName,
			final String[] propertyNames, final ValueMap properties) throws XMLStreamException {
		for (String prop : propertyNames) {
			String value = properties.get(prop, String.class);
			if (value != null) {
				writeElement(stream, elementName, value);
				break;
			}
		}
	}

	private void writeElement(final XMLStreamWriter stream, final String elementName, final String text)
			throws XMLStreamException {
		stream.writeStartElement(NS, elementName);
		stream.writeCharacters(text);
		stream.writeEndElement();
	}

	

}
